% Created 2021-04-19 Mon 13:53
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,letterpaper]{article}
               \usepackage{pgfplots}
               \usepackage{microtype,tikz,fancyhdr,amsthm}
\usepackage[framemethod=tikz]{mdframed}
\mdfdefinestyle{definition}{linecolor=black,frametitle=Definition,backgroundcolor=gray!20,roundcorner=10pt,linewidth=1.5pt,frametitlerule=true,skipabove=5pt,skipbelow=5pt,rightmargin=15pt,leftmargin=15pt}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{geometry,siunitx}
\geometry{bottom=25mm}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\leftmark}
\fancyhead[R]{\thepage}
\fancyfoot[C]{}
\colorlet{cyanish}{cyan!25!}
\usetikzlibrary{calc}
\author{Devyn Barrie\thanks{barr0554@algonquinlive.com}}
\date{\today}
\title{Ratio, Proportion and Variation\\\medskip
\large Wednesday Math Class, Week 14 (Professor Calvin Climie)}
\hypersetup{
 pdfauthor={Devyn Barrie},
 pdftitle={Ratio, Proportion and Variation},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.5)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents

\mdfdefinestyle{examples}{linecolor=black,backgroundcolor=cyanish,roundcorner=10pt,linewidth=1.5pt,frametitlerule=true,frametitle=Examples,skipabove=5pt,skipbelow=5pt,rightmargin=15pt,leftmargin=15pt}

\mdfdefinestyle{equation}{hidealllines=true,leftline=true,rightline=true,linecolor=blue,linewidth=1.5pt,skipabove=5pt,skipbelow=5pt,rightmargin=15pt,leftmargin=15pt}

\section{Ratios}
\label{sec:orgb197126}

\begin{itemize}
\item A ratio is a quotient.
\item Typically ratios compare quantities of the same kind or unit.
\item If a ratio compares different quantities, it is called a \textbf{\textbf{rate}}.
\item \textbf{\textbf{Proportion}}: When two ratios are set equal to each other, it is called a \textbf{\textbf{proportion}}.
\begin{itemize}
\item For example, the percent formula.
\end{itemize}
\end{itemize}

\[\dfrac{p}{b} = \dfrac{r}{100}.\]

\begin{itemize}
\item With a proportion you can cross-multiply, which helps to solve for an unknown (See Fig. 1).
\end{itemize}

\begin{figure}[t]
\centering
\begin{tikzpicture}
%\draw[gray, step=0.25] (0,0) grid (2,2);
%\foreach \x in {0,0.25,1,...,2}{\node[below] at (\x,0){\x}; }
%\foreach \y in {0,0.25,1,...,2}{\node[left] at (0,\y){\y}; }
\node at (1,1){\LARGE \(\dfrac{a}{c} = \dfrac{b}{d}.\)};
\draw[thick,red,-stealth] (0.6,1.25) -- (1.275,0.75);
\draw[thick,red,-stealth] (0.6,0.625) -- (1.275,1.25);
\end{tikzpicture}
\caption{The cross multiplication property of ratios allows this to also be written as $ad=bc$.}
\end{figure}

\begin{mdframed}[style=examples]
Express the ratio $\SI{210}{\metre}$ to $\SI{18}{\km}$ in simplest form.

\[\dfrac{\SI{210}{\metre}}{\SI{3}{\km}} = \dfrac{\SI{21}{\metre}}{\SI{1800}{\metre}} = \dfrac{\SI{7}{\metre}}{\SI{600}{\metre}}.\]

\end{mdframed}


\section{Variation}
\label{sec:org33fa137}

\subsection{Direct Variation}
\label{sec:orgb585e70}
\begin{itemize}
\item When the ratio of two variables always remains the same.
\item That is, the ratio of the variables is equal to a constant, \(k\).
\end{itemize}

\[\dfrac{a}{b} = k.\]

\begin{itemize}
\item A special symbol is used to denote a relationship of direct variation: \(\propto\).
\item A simple rearrangement allows conversion of a proportion to an equation.
\end{itemize}

\begin{mdframed}[style=equation]
\begin{align*}
a &\propto b.\\
a &= kb.
\end{align*}
\end{mdframed}

\subsection{Inverse Variation}
\label{sec:org39af80b}
\begin{itemize}
\item Inverse variation is when the product of two variables is the same.
\begin{itemize}
\item ``\(y\) is inversely proportional'' to \(x\).
\end{itemize}
\end{itemize}

\begin{mdframed}[style=equation]
\[y \propto x^{-1}.\]
\end{mdframed}

\subsection{Joint Variation}
\label{sec:orgf6cfb38}
\begin{itemize}
\item Like direct variation but multivariate.

\[a\propto zb.\]
\end{itemize}

\section{Error}
\label{sec:org295986f}
\begin{itemize}
\item Error measures the uncertainty of a value. An error could arise due to precision of the measurement equipment or due to variation in the population or fluctuation in the value being measured.
\item Error is not the same as a mistake in measuring.
\item During calculations, avoid rounding numbers as the rounding will accumulate. At a minimum, carry one or two extra significant figures through the calculations.
\item An observed/calculated value is a value obtained from observations.
\item True value is a hypothetical ``absolutely true'' measurement that may or may not exist in reality. It isn't really possible for us to get this value, but we can take an average of many measurements to stand in for it.
\item The error in a measurement is given by.
\end{itemize}

\begin{mdframed}[style=equation]
\[\text{Error} = |\text{correct measurement} - \text{determined measurement}|.\]
\end{mdframed}

\begin{itemize}
\item The percent error is given by.
\end{itemize}

\begin{mdframed}[style=equation]
\[\text{Percent error} = \dfrac{\text{error}}{\text{correct measurement}} \cdot 100.\]
\end{mdframed}

\begin{itemize}
\item Accuracy is a measure of how close the observed value is to the ``true'' value.
\item Precision refers to the number of significant figures.
\end{itemize}
\end{document}
