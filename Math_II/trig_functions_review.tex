\documentclass[10pt,letterpaper]{article}

\title{Trigonometric Functions Review}
\author{Devyn Barrie}
\usepackage[T1]{fontenc}
\usepackage{isomath}
\usepackage{nicefrac}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{tikz,microtype,amsthm,float,amstext,array,hyperref,pgfplots,fontspec,graphicx,pdfpages,hyperref,fancyhdr,nameref}
\newfontfamily\myfont{Casio fx-9860GII}
\renewenvironment{proof}{{\bf \emph{Solution.} }}{\hfill $\blacksquare$ \\}
\DeclareMathOperator{\arcsec}{arcsec}
\DeclareMathOperator{\arccsc}{arccsc}
\DeclareMathOperator{\arccot}{arccot}
\usepackage{geometry}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{}
\fancyhead[C]{Trigonometric Functions Review}
\fancyhead[R]{\thepage}
\geometry{bottom=25mm}
\begin{document}
	
\maketitle

\tableofcontents

\pagebreak

\section{Stuff to Know}

\subsection{Radians}

The radian is the SI unit for angle measure. It is a dimensionless value, so units don't need to be specified when using radians, though if you want you can use ``rad", r, or a superscripted c. The radian is defined as the angle subtended from the centre of a circle which intercepts an arc equal in length to the radius of the circle. Radians are used for angle measure in calculus because they are nicer to work with than degrees.

The relationship between a circle's radius, $r$, arc length, $s$ and central angle, $\theta$, is characterized by the equation,

\begin{equation}
\theta = \dfrac{s}{r}.
\end{equation}

The unit circle has a radius of 1 unit, so equation (1) reduces to

\begin{equation}
\theta = s.
\end{equation}

Sometimes equation (1) is given in the following form, but of course is easily re-arranged with algebra.

\begin{equation}
	s = r\theta.
\end{equation}

\subsection{Trig Functions}

The trigonometric functions can be defined a few ways. 

The most elementary way of defining the trig functions is as ratios of lengths of triangles. In this way, the function takes an angle and gives you back the side of a triangle.

\begin{align*}
	\sin\theta &=\dfrac{\text{opp}}{\text{hyp}}\\
	\cos\theta &=\dfrac{\text{adj}}{\text{hyp}}\\
	\tan\theta &=\dfrac{\text{opp}}{\text{adj}}
\end{align*}

Another way to define them is with the equation of a circle.

\begin{equation*}
	r^2=x^2+y^2.
\end{equation*}

The trig functions are then defined as ratios of these variables. The basic idea is you draw a right triangle within the circle, and the sides form certain ratios. Put any number into the variables and you get back the value of that function. To get $\theta$ you use an inverse function like $\arcsin$ or $\arccos$.

\begin{align*}
	\sin\theta &=\dfrac{y}{r} &\csc\theta &=\dfrac{r}{y}\\
	\\
	\cos\theta &=\dfrac{x}{r} &\sec\theta &=\dfrac{r}{x}\\
	\\
	\tan\theta &=\dfrac{y}{x} &\cot \theta &=\dfrac{x}{y}
\end{align*}

It is important to remember that this means sine is associated with \textit{y}, cosine is associated with \textit{x}, and tangent is associated with both. 

You may notice that the functions on the right column are just upside down versions of the ones in the left column. In fact, they are called {\bfseries reciprocal trig functions}. The original three are known as the primary trig functions. The names of the reciprocals are {\bfseries cosecant} (reciprocal of sine), {\bfseries secant} (reciprocal of cosine) and {\bfseries cotangent} (reciprocal of tangent). An alternative definition of the reciprocals is to simply take the primary function and place it under 1. E.g., \(\csc \theta \equiv \nicefrac{1}{\sin \theta}.\)

One thing to know is that $\sin \theta$ and $\cos \theta$ are defined \emph{for all real numbers}. Tangent and the reciprocal functions are not. The graphs printed in the appendix give you an idea of where these functions are undefined; see also the \nameref{properties} section for complete details. 


Note that the reciprocal functions are \emph{not} the same as the inverse functions. The inverse functions are usually written with ``arc" in front of their name, but can also be expressed with a \(-1\) as a superscript (e.g. \(\sin^{-1}\theta\)). This is \emph{not} an exponent, it is merely notation. This document will stick to using the ``arc" preface to indicate inverse functions.

\subsection{Inverse Trig Functions}

\begin{align*}
	\sin \theta &\to \arcsin \theta &\sec \theta &\to \arcsec \theta\\
	\tan \theta &\to \arctan \theta &\csc \theta &\to \arccsc \theta \\
	\cos \theta &\to \arccos \theta &\cot \theta &\to \arccot \theta 
\end{align*}

Again, the inverses are not the same as the reciprocals. Meaning, \(sin^{-1}\theta \not\equiv \nicefrac{1}{sin \theta}\). 

The domain of the inverse functions is the same as the range of the function it is inverse to. For example, the range of sine is \([-1,1]\), so the domain of arcsin is \([-1,1]\). If you try to put 2 into arcsin, your calculator will complain. 


\begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\definecolor{lcd}{rgb}{0.35,0.53,0.24}
	\path[draw,fill,lcd](0,0) rectangle (9,2);
	\node[font=\myfont \scriptsize] at (5,1){ERROR: Are you some kind of an idiot?};
	\node[font=\myfont] at (6.5,1.7) {\scriptsize Rad};
	\node[font=\myfont] at (7.5,1.7){\scriptsize M};
\end{tikzpicture}
\end{figure}

\subsection{Unit Circle}

The unit circle is a practical tool for solving problems. It is a circle with a radius of 1 unit, and examples as to its applications are shown in the solutions. An annotated unit circle with angles and function values is included in appendix A. 

\subsection{Signs of Trig Functions}

The signs of a function are different depending on the quadrant the function is in. You can recall the correct signs for each quadrant with the ``All Students Take Calculus" rule, illustrated in fig. \ref{fig:cast}.

  \begin{figure}[t]
	\centering
	\begin{tikzpicture}[scale=1.2]
		\draw
		circle (3);
		\draw
		(0,-3) -- (0,3);
		\draw
		(-3,0) -- (3,0);
		\node at (1,1){All};
		\node at (-1,1){Students};
		\node at (-1,-1){Take};
		\node at (1,-1){Calculus};
		\draw[thick, ->] (1,1.5) arc (0:180:1);
		\draw[thick, ->] (-1,-1.5) arc (0:180:-1);
	\end{tikzpicture}
	\caption{``All Students Take Calculus" helps you recall what sign the functions have on the cartesian plane.} \label{fig:cast}
\end{figure}

In quadrant I, they are all positive. In quadrant II, sine is positive. In quadrant III, tangent is positive. In quadrant IV, cosine is positive.

Finding a reference angle ($\beta$) helps you determine all possible values of $\theta$. The relationship between the reference angle and $\theta$ in all four quadrants is characterized as:

\begin{align*}
	&\theta_{I} = \beta.\\
	&\theta_{II} = \pi-\beta.\\
	&\theta_{III} = \pi +\beta.\\
	&\theta_{IV}=2\pi - \beta.
\end{align*}

\subsection{Special Triangles}

There are two special triangles that are helpful for finding exact values of trigonometric functions. In terms of degrees, they are called the 30-60-90 and the 45-45-90 triangles. They are presented below using radians, in fig, \ref{fig:triangles}. Conversion of radians to degrees is left as an exercise to the reader.

\begin{figure}[h]
	\centering
	\includegraphics[width=8cm]{/home/adidas/Documents/Math/Graphics/special_triangles.pdf}
\caption{The special triangles}\label{fig:triangles}
\end{figure}

Please note that no efforts were expended to make the triangles have realistic-appearing dimensions.

\section{Finding Values for Theta}

There are few cases to consider:

\begin{enumerate}
	\item Find $\theta$ given the value of one function and possibly a range (examples 1(1), 1(2) and 1(3)).
	\item Find $\theta$ knowing the value of one function and the sign of another (example 1(4)). 
	\item Find $\theta$ given a point the terminal side passes through. (example 1(5)). 
\end{enumerate}

1(1). \textit{Find values for $\theta$ if $ \cos\theta = -0.3178 $ and $ 0 \leq \theta \leq 2\pi $}.

\begin{proof}

  \begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\draw
		circle (3);
		\draw
		(0,-3) -- (0,3);
		\draw
		(-3,0) -- (3,0);
		\draw[->] (0,0) -- (-2.5,2.5);
		\draw[->] (0,0) -- (-2.5,-2.5);
		\draw[thick, ->] (1,0) arc (0:135:1); %theta 1
			\path (1.2,0.2) node at (0.6,0.2) {$\theta_{II}$}; %label for theta 1
		\draw[red,thick, ->] (-0.707,0.707) node at (-0.6,0.2) {\(\beta\)} arc [start angle=135, end angle=180, radius=1]; %beta
		\draw[thick, ->] (1.5,0) arc (0:225:1.5); %theta 2
			\path (1.7,0.2) node at (2,0.2) {$\theta_{III}$}; %label for theta 2
	\end{tikzpicture}
\caption{It is most helpful to sketch a figure of the possible angles. In this case, there are two possible angles, $\theta_1$ and $\theta_2$. The red arc indicates $\beta$, or the reference angle.} \label{fig:circ1}
\end{figure}

There are two solutions to cosine within $ [0,2\pi] $, and cosine has negative values in quadrants II and III. Sketch terminal arms as shown in fig. \ref{fig:circ1} and note the acute reference angle shown in red. I would, perhaps imprecisely, describe the reference angle as representing the angle of a triangle that exists in quadrant II which can be reflected into quadrant III with nothing but a sign change for the opposite side. Geometrically, this is a good way to view it. If you reflect a triangle across the x-axis on a cartesian plane, the base (x-value) doesn't change, but your opposite side (y-value) needs to change sign to negative. 


The reference angle, called $\beta$, is found in this case using $\arccos |-0.3178|$. The absolute value should be always used to ensure the reference angle is found.

\[ \arccos|-0.3178| \approx 1.2474 \]

Once the reference angle is known, the sought angles are found,

\[ \theta_{II} \approx \pi - 1.2474 \approx 1.8942.\]
\[ \theta_{III} \approx \pi + 1.2474 \approx 4.3890. \]

\end{proof}

1(2). \textit{Find the \textbf{exact} values of $\theta$ for the interval $ [0,2\pi] $ knowing $ \tan\theta = -\sqrt{3}. $}

\begin{proof}
	Tangent has a negative value in quadrants II and IV, so draw terminal arms there (fig. \ref{fig:circ2}).
	
	 \begin{figure}[t]
		\centering
		\begin{tikzpicture}
			\draw
			circle (3);
			\draw
			(0,-3) -- (0,3);
			\draw
			(-3,0) -- (3,0);
			\draw[<->] (-2.5,2.5) -- (2.5,-2.5);
			\draw[thick, ->] (1,0) arc (0:135:1); %theta 1
			\path (1.2,0.2) node at (0.7,0.2) {$\theta_I$}; %label for theta 1
			\draw[red,thick, ->] (-0.707,0.707) arc [start angle=135, end angle=180, radius=1] node at (-0.6,0.2){\(\beta\)}; %beta
			\draw[thick, ->] (1.5,0) arc (0:315:1.5); %theta 2
			\path (1.7,0.2) node at (1.8,0.2){$\theta_{II}$}; %label for theta 2
		\end{tikzpicture}
	\caption{A sketch of the angles involved.} \label{fig:circ2}
	\end{figure}
	The process here is similar to example 1, except an exact value is required. So put away the scientific calculator! 
	
	Instead, finding $\beta$ requires knowledge of the special triangles. In particular, $ \tan\nicefrac{\pi}{3}=\sqrt{3}. $ Therefore, $ \beta = \nicefrac{\pi}{3}. $
	
	Again, very similar to example 1, the two possible angles are found as,
	
	\[ \theta_1 = \pi - \dfrac{\pi}{3} = \dfrac{2\pi}{3}. \]
	\[ \theta_2 = 2\pi - \dfrac{\pi}{3} = \dfrac{5\pi}{3}. \]
\end{proof}

1(3). \textit{Find values for $\theta$ in the range $ \left[-\dfrac{\pi}{2}, \dfrac{5\pi}{2}\right] $ if $ \cos\theta = 0.2. $}

\begin{proof}

The range required is one full rotation of a circle, plus or minus $ \nicefrac{\pi}{2} $. I would expect four solutions. There will be two ``initial" solutions, one in quadrant I and the other in quadrant IV, plus another in ``quadrant V" and ``quadrant -I" (not sure what else to call them!)

The usual method will work here. 

\begin{align*}
&\beta = \theta_{I} = \arccos 0.2 \approx 1.369.\\
&\theta_{IV} \approx 2\pi - 1.369 \approx 4.914.
\end{align*}


Those are the approximate values of the angles within the range $ [0,2\pi]. $

You can expect another two solutions because to go to $ \nicefrac{5\pi}{2} $, you need to go all around the circle from 0 and add another $ \nicefrac{\pi}{2} $, which means you are coterminal to the quadrant I solution. I will call this place ``quadrant V", for five. The other solution will have you go $ -\nicefrac{\pi}{2} $ from 0. 

Finding an angle in quadrant IV uses the equation $ \theta_{IV} = 2\pi - \beta $, so it follows that finding an angle in quadrant V would probably use $ 2\pi+\beta $.

\[ \theta_V \approx 2\pi + 1.369 \approx 7.652 .\]

And to find an angle in quadrant -I, my guess was just to use $ -\beta $, since normally $ \theta_{I} =\beta.$

\[ -\beta = -1.369. \]

Hence the full approximate solutions are,

\begin{align*}
	&\theta_{I} \approx 1.369\\
	& \theta_{IV} \approx 4.914\\
	& \theta_V \approx 7.652\\
	&\theta_{-I} \approx -1.369.
\end{align*}
\end{proof}

1(4). \textit{Find \(\theta\) given that \(\tan \theta = -0.817\) and the sign of \(\csc \theta\) is positive.}

\begin{proof}
	\(\tan \theta = -0.817 \implies \theta \in Q_{II}, Q_{IV}\). 
	
	As well, since \(\csc \theta = \nicefrac{r}{y}\), it being positive implies \(\theta \in Q_{I}, Q_{II}\). 
	
	The common thread here is quadrant II, so that will be the focus of solution.
	
	Rounding the approximate answer to four decimal places, we have the following.
	
\[\theta_{II} = \pi - \arctan|-0.817| \approx 2.4566.\]
	
	This satisfies the restrictions given. 
\end{proof}

1(5). \textit{Find sine, cosine and tangent of an angle whose terminal side passes through the point $ (17,15) $.}

\begin{proof}
	Use Pythagoras to find $r$.
	
	\[\sqrt{r^2} = \sqrt{17^2+15^2} = \sqrt{514} .\]
	
	The radical is a surd.
	
	Recalling the definitions of the trig functions given earlier in the document, it is concluded that
	
\begin{align*}
&\sin \theta = \dfrac{15}{\sqrt{514}} = \dfrac{15\sqrt{514}}{514}.\\
&\cos \theta = \dfrac{17}{\sqrt{514}} = \dfrac{17 \sqrt{514}}{514}.\\
&\tan \theta = \dfrac{15}{17}.
\end{align*}
	
\end{proof}
	
	\section{Finding Values of Functions}
	
	In the last section, examples concerned how to determine what the angle of a function is. This section focuses on finding the value of a function \emph{knowing} the angle.
	
	There are probably three main scenarios:
	
	\begin{enumerate}
		\item Trivial cases where you can just straight-up plug it into your calculator. Not much to this. (I will not bother with examples.)\\
		\item An exact closed-form answer is required. (Example 2(1), 2(2), 2(3).)  \\
		\item You want to know when one function is equivalent to its cofunction. (Example 2(4).)
	\end{enumerate}

2(1). \textit{Find exact values for $ \sin \dfrac{3\pi}{4} and \cos \dfrac{3\pi}{4}. $}

\begin{proof}
	This is a good application of the addition identities,
	
	\[ \sin(\alpha + \beta) \equiv \sin \alpha \cos \beta + \cos \alpha \sin \beta . \]
	\[ \cos (\alpha+\beta) \equiv \cos \alpha \cos \beta - \sin \alpha \sin \beta. \]
	
	It is needed to find two angles whose sine and cosine are already known, and that add up to $ \dfrac{3\pi}{4} $. (It may be easier to figure this out in terms of degrees.)
	
	Such values are $ \dfrac{\pi}{2} $ and $ \dfrac{\pi}{4} $. The sine and cosine of these angles are known from the special triangles. (Note that $\cos \dfrac{\pi}{2} =0 $, due to the definition $\cos = \dfrac{x}{r}.$ For that angle the x-value is zero since it goes straight up on the unit circle.)
	
	Use the identity formula,

\begin{align*}
&\sin\left(\dfrac{\pi}{2} + \dfrac{\pi}{4} \right) \equiv 1\left(\dfrac{\sqrt{2}}{2}\right) + 0\left(\dfrac{\sqrt{2}}{2}\right) \equiv \dfrac{\sqrt{2}}{2}.\\
&\cos\left(\dfrac{\pi}{2} + \dfrac{\pi}{4}\right) \equiv  0\left(\dfrac{\sqrt{2}}{2}\right) - 1\left(\dfrac{\sqrt{2}}{2}\right) \equiv -\dfrac{\sqrt{2}}{2}.
\end{align*}

	\begin{align*}
		&\therefore \sin\left(\dfrac{3\pi}{4}\right) = \dfrac{\sqrt{2}}{2}.\\
		&\therefore \cos\left(\dfrac{3\pi}{4}\right) = -\dfrac{\sqrt{2}}{2}.
	\end{align*}
\end{proof}

2(2). \textit{Find the exact value of \(\cos \nicefrac{5\pi}{6}\)}.

\begin{proof}
	
Switching over to degrees to make this a little easier, we have the following.

\[\dfrac{5\pi}{6} = 150^{\circ} = 90^{\circ}+60^{\circ}.\]

Therefore, use the sum identity for cosine to add $\nicefrac{\pi}{2}$ and $\nicefrac{\pi}{3}$.

	\[\cos(\alpha + \beta) \equiv \cos \alpha \cos \beta - \sin \alpha \sin \beta.\]
	
	Where \(\alpha = \nicefrac{\pi}{2}\) and \(\beta = \nicefrac{\pi}{3}.\) Since \(\cos \nicefrac{\pi}{2} = 0\), the equation becomes:
	
	\[\cos\left(\dfrac{\pi}{2} + \dfrac{\pi}{3} \right) \equiv 0 - \sin \dfrac{\pi}{2} \sin \dfrac{\pi}{3}.\]
	
	Either by general knowledge or using the special triangles, one can find that \(\sin \nicefrac{\pi}{2} = 1\) and \(\sin \nicefrac{\pi}{3} = \nicefrac{\sqrt{3}}{2}.\)
	
	\[\therefore \cos\left(\dfrac{\pi}{2} + \dfrac{\pi}{3} \right) \equiv -1\left(\dfrac{\sqrt{3}}{2}.\right) = -\dfrac{\sqrt{3}}{2}.\]
\end{proof}

2(3). \textit{Find \(\cos 2\theta\) if \(\sin\theta = \nicefrac{5}{8}\)}.

\begin{proof}
	One form of the cosine double-angle identity is,
	
	\[\cos 2\theta = 1-2\sin^2 \theta.\]
	
	Since we know \(\sin \theta = \nicefrac{5}{8}\), put that into the formula.
	
	\[\cos 2\theta = 1-2\left(\dfrac{5}{8}\right)^2 = 1-2\left(\dfrac{25}{64}\right) = 1-\dfrac{25}{32} = \dfrac{7}{32}.\]
	
	\[\therefore \cos 2\theta = \dfrac{7}{32}.\]
\end{proof}

2(4). \textit{Find the value of $\theta$ such that $\cot \theta$ = $\tan \nicefrac{\pi}{8}.$}

\begin{proof}
	
There is an identify that relates each trig function to its cofunction (i.e. sine to cosine, tangent to cotangent, secant to cosecant.)

It goes like,

\[ f(x) = g\left(\dfrac{\pi}{2} - x\right). \]

\[ \therefore \tan\dfrac{\pi}{8} = \cot\left(\dfrac{\pi}{2} - \dfrac{\pi}{8}\right) = \cot\left(\dfrac{3\pi}{8}\right).\]

\end{proof}

\section{Properties of Trig Functions}
\label{properties}

\begin{minipage}[c]{\textwidth}
\begin{minipage}[l]{0.45\textwidth}
Sine

\begin{itemize}
	\item Period of \(2\pi\).
	\item Integer solution: \(\theta = 0\).
	\item Zero at all integer multiples of $\pi$.
	\item Amplitude of 1.
	\item Odd function, as \(\sin(-\theta) = -\sin \theta\)
	\item Domain: \((-\infty, \infty)\).
	\item Range: \([-1,1]\).
\end{itemize}
\end{minipage}
\begin{minipage}[r]{0.45\textwidth}
Cosine

\begin{itemize}
	\item Period of \(2\pi\).
	\item No integer solutions.
	\item Zero at $n\pi - \nicefrac{\pi}{2}$, $n \in \mathbb{Z}$.
	\item Amplitude of 1.
	\item Even function as \(\cos (-\theta) = \cos \theta\).
	\item Domain: \((-\infty, \infty)\).
	\item Range: \([-1,1]\).
\end{itemize}
\end{minipage}
\end{minipage}
\begin{minipage}[c]{\textwidth}
	\vspace{1em}
	\begin{minipage}[l]{0.45\textwidth}

		Tangent
		
		\begin{itemize}
			\item \(\tan \theta \equiv \dfrac{\sin \theta}{\cos \theta}\).
			\item Period of \(\pi\).
			\item Amplitude n/a --- not sinusoidal.
			\item Integer solution: \(\theta = 0\).
			\item Zero at integer multiples of $\pi$.
			\item Undefined where \(\cos\theta = 0\).  
			\item Odd, because \(\tan (-\theta) \equiv -\tan(\theta).\)
			\item Domain: all real numbers except odd multiples of $\nicefrac{\pi}{2}$.
			\item Range: \((-\infty,\infty)\).
		\end{itemize}
	\end{minipage}
\begin{minipage}[r]{0.45\textwidth}
		Cosecant

\begin{itemize}
	\item \(\csc \theta \equiv \dfrac{1}{\sin \theta}\).
	\item Period of \(2\pi\).
	\item No solutions.
	\item Undefined where \(\sin\theta = 0\). 
	\item Minima where sine has maxima; maxima where sine has minima.
	\item Odd, because \(\csc (-\theta) \equiv -\csc(\theta).\)
	\item Domain: all real numbers except $n\pi$, $n \in \mathbb{Z}$.
	\item Range: \((-\infty,-1] \cup [1,\infty)\).
\end{itemize}
\end{minipage}
\end{minipage}

\begin{minipage}[c]{\textwidth}
	\begin{minipage}[l]{0.45\textwidth}
		Secant
		
		\begin{itemize}
			\item \(\sec \theta \equiv \dfrac{1}{\cos \theta}\).
			\item Period of $2\pi$.
			\item No solutions.
			\item Undefined where $\cos \theta = 0$.
			\item Minima where cosine has maxima; maxima where cosine has minima.
			\item Even.
			\item Domain: All real numbers except $\nicefrac{\pi}{2} + n\pi$, $n \in \mathbb{Z}$.
			\item Range: $(-\infty, -1] \cup [1,\infty)$.
		\end{itemize}
	\end{minipage}
\begin{minipage}[r]{0.45\textwidth}
	Cotangent
	\begin{itemize}
	\item \(\cot \theta \equiv \dfrac{1}{\tan \theta}\).
	\item Period of $\pi$.
	\item No integer solutions.
	\item Zero at $n\pi + \nicefrac{\pi}{2}$, $n \in \mathbb{Z}$.
	\item Undefined where $\tan \theta = 0$.
	\item Odd.
	\item Domain: All real numbers except $n \pi$, $n \in \mathbb{Z}$.
	\item Range: $(-\infty,\infty)$.
	\end{itemize}
\end{minipage}
\end{minipage}


\section{Trig Identities}

\subsection{What the heck is this: $\mathbf{\equiv}$}

When working with identities, you will see a different kind of equals sign: the triple-bar ($\equiv$), also known as the ``equivalence" symbol. The difference is the normal equals sign simply states that two things are equal for some reason. For example, the equation

\[2x^2+3x-9 = 0,\]

is basically telling us that the left-hand side (LHS) is equal to zero at some point. There is a value of $x$ that makes it so, but it's not always going to be the case. 

The equivalence symbol means that the statement is identically equal to something else. This is a special kind of relationship that holds for all cases in a given domain. For example, the identity

\[\sin^2 \theta + \cos^2 \theta \equiv 1,\]

is true for all $\theta \in \mathbb{R}$. You can try any real number you want, it will always equal 1. In summary, only use $\equiv$ for relations that are true in all cases for the domain. 

\subsection{Important Identities}

{\bfseries Pythagorean Formula for Sines and Cosines}

\[\sin^2\theta + \cos^2\theta \equiv 1.\]

{\bfseries Addition and Subtraction for Sines and Cosines}

\begin{align*}
	&\sin(\alpha + \beta) \equiv \sin\alpha \cos\beta + \sin\beta \cos \alpha.\\
	&\sin(\alpha - \beta) \equiv \sin\alpha \cos\beta - \sin\beta \cos \alpha.\\
	&\cos(\alpha + \beta) \equiv \cos\alpha \cos\beta - \sin \alpha \sin \beta.\\
	&\cos(\alpha - \beta) \equiv \cos\alpha \cos \beta + \sin \alpha \sin \beta.
\end{align*}

{\bfseries Periodicity}

\begin{align*}
	&\sin(\theta + 2\pi) \equiv \sin \theta. &\csc(\theta + 2\pi) \equiv \csc\theta.
	\\
	&\cos(\theta + 2\pi) \equiv \cos \theta. &\sec(\theta+2\pi) \equiv \sec\theta.
	\\
	&\tan(\theta + \pi) \equiv \tan \theta. &\cot(\theta + \pi) \equiv \cot \theta.
\end{align*}

{\bfseries Even and Oddness}

\begin{align*}
	&\sin(-\theta) \equiv -\sin \theta.\\
	&\cos(-\theta) \equiv \cos \theta.\\
	&\tan(-\theta) \equiv -\tan \theta.
\end{align*}

{\bfseries Conversion via Complementary Angle}

Each function can be changed into its cofunction with the same formula. I will just use sine and cosine as the example.

\begin{align*}
	&\sin \theta \equiv \cos\left(\dfrac{\pi}{2} - \theta \right).\\
	&\cos \theta \equiv \sin\left(\dfrac{\pi}{2} - \theta \right).
\end{align*}

{\bfseries More identities}

\href{https://www2.clarku.edu/faculty/djoyce/trig/identities.html}{More identities are listed here.}

\subsection{Derivation of Certain Identities}

{\bfseries Double Angle}

Double angle formulas are derived using the addition formulas. Instead of $\alpha$ and $\beta$ we just use $\theta$.

For sine:

\[\sin \theta \cos \theta + \cos \theta \sin \beta \equiv \sin \theta (\cos \theta + \cos \theta) \equiv 2\sin\theta \cos \theta.\]

For cosine:

\[\cos \theta \cos \theta - \sin \theta \sin \theta \equiv \cos^2 \theta - \sin^2 \theta\]

Using the Pythagorean identity, this can be changed into two other forms.

\[\cos 2\theta \equiv 2\cos^2 \theta - 1 \equiv 1 - 2\sin^2 \theta.\]

{\bfseries Half-Angle}

Half angle formulae come from the cosine double angle identity.

Let $A = \nicefrac{\theta}{2}$. 

\begin{align*}
\cos 2A &\equiv 2\cos^2 A - 1\\
\cos 2A + 1 &\equiv 2 \cos^2 A\\
\dfrac{\cos 2A + 1}{2} &\equiv cos^2 A
\end{align*}

\[\therefore \cos \dfrac{\theta}{2} \equiv \sqrt{\dfrac{\cos \theta + 1}{2}}.\]

For the sine:

\begin{align*}
\cos 2A &\equiv 1-2\sin^2A\\
2\sin^2A  &\equiv 1-\cos2A\\
\sin^2 A &\equiv \dfrac{1-\cos 2A}{2}\\
\end{align*}

\[\therefore \sin \dfrac{\theta}{2} \equiv \sqrt{\dfrac{1-\cos \theta}{2}}.\]

	\includepdf{/home/adidas/Documents/Math/Graphics/unit-circle.pdf}


	\includepdf{/home/adidas/Documents/Math/Graphics/trig_graphs.pdf}


\pagebreak

\end{document}
