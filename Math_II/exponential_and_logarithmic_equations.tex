% Created 2021-04-13 Tue 21:17
% Intended LaTeX compiler: xelatex
\documentclass[10pt,letterpaper]{article}
               \usepackage{pgfplots}
               \usepackage{microtype,tikz,fancyhdr,amsthm}
\usepackage[framemethod=tikz]{mdframed}
\mdfdefinestyle{definition}{linecolor=black,frametitle=Definition,backgroundcolor=gray!20,roundcorner=10pt,linewidth=1.5pt,frametitlerule=true,skipabove=5pt,skipbelow=5pt,rightmargin=15pt,leftmargin=15pt}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\renewenvironment{proof}{{\bf \emph{Solution.} }}{\hfill $\blacksquare$ \\}
\usepackage{geometry}
\pagestyle{fancy}
\fancyhead[L]{\leftmark}
\fancyhead[R]{\thepage}
\fancyfoot[C]{}
\geometry{bottom=25mm}
\usepackage{float,microtype,commath,siunitx}
\colorlet{cyanish}{cyan!25!}
\pgfplotsset{compat=1.17}
\author{Devyn Barrie\thanks{barr0554@algonquinlive.com}}
\date{\today}
\title{Exponential and Logarithmic Functions}
\hypersetup{
 pdfauthor={Devyn Barrie},
 pdftitle={Exponential and Logarithmic Functions},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.5)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{The Exponential Function}
\label{sec:orgf033c03}

Exponential functions are equations of the form

\[y=ab^x.\]

Where \(a\) is a fixed number known as the y-intercept and \(b\) is a fixed number called the \emph{base}. As one can see, an exponential equation is one where the independent variable is the exponent. The dependent variable then changes according to a consistent ratio. The base is normally restricted to positive real numbers.\footnote{Negative bases are possible with imaginary numbers. \href{https://www.wolframalpha.com/input/?i=y\%3D\%28-2\%29\%5Ex}{Example: Wolfram Alpha}}


For example, the exponential equation \(y=2^x\) has this table of values:

\begin{center}
\begin{tabular}{rr}
\(x\) & \(y\)\\
\hline
-2 & 0.25\\
-1 & 0.5\\
0 & 1\\
1 & 2\\
2 & 4\\
3 & 8\\
4 & 16\\
\end{tabular}
\end{center}


\mdfdefinestyle{examples}{linecolor=black,backgroundcolor=cyanish,roundcorner=10pt,linewidth=1.5pt,frametitlerule=true,frametitle=Examples,skipabove=5pt,skipbelow=5pt,rightmargin=15pt,leftmargin=15pt}

\mdfdefinestyle{tip}{linecolor=black,backgroundcolor=gray!25!,roundcorner=10pt,linewidth=1.5pt,frametitlerule=true,skipabove=5pt,skipbelow=5pt,rightmargin=15pt,leftmargin=15pt}

\mdfdefinestyle{equation}{hidealllines=true,leftline=true,rightline=true,linecolor=blue,linewidth=1.5pt,skipabove=5pt,skipbelow=5pt,rightmargin=15pt,leftmargin=15pt}

\begin{mdframed}[style=tip]
Did you notice how the exponential equation \(y=2^x\) always increases by a compounding ratio that started at 0.25? With every integer step of \(x\), it increases by another quarter over what its previous amount of increase was.
\end{mdframed}

\subsection{Y-intercept}
\label{sec:orga5bb947}

The \(a\) value is a multiplier of the function that sets the y-intercept. An example of this is seen in the compound interest formula, where \(a\) is denoted \(P\), representing principle.

\begin{mdframed}[style=equation]

\[I=P\left(1+\dfrac{r}{n}\right)^{nt}.\]

Where \(I =\) Interest, \(P\) = Principle, \(r\) = rate, \(n\) = compounding periods, \(t\) = time.
\end{mdframed}

Unlike the base, \(a\) is allowed to be negative. In such a case, the graph flips in what is called a \textbf{reflection} across the x-axis. Of course, \(a\) does not always have to be present in an exponential equation. The bare-bones form is just \(y=b^x\). See Fig. \ref{fig:1} for examples of how this parameter affects the graph.

\begin{figure}
\centering
\begin{tikzpicture}
\begin{axis}[
width=10cm,
clip=false,
legend pos=outer north east,
axis lines = center,
xlabel=\(x\),
ylabel=\(y\),
]
\addplot[thick,<->,
domain=-4:4,
samples=300,
color=red,
thick,
] {2*2^x};
\addlegendentry{\(y=2(2)^x\)}
\addplot[thick,<->,
domain=-4:4,
samples=300,
color=blue,
thick,
] {-2*2^x};
\addlegendentry{\(y=-2(2)^x\)}
\addplot[thick,<->,
domain=-4:4,
samples=400,
color=black,
thick,
] {4*2^x};
\addlegendentry{\(y=4(2)^x\)}
\addplot[thick,<->,
domain=-4:4,
samples=300,
color=brown,
thick,
] {-4*2^x};
\addlegendentry{\(y=-4(2)^x\)}
\end{axis}
\end{tikzpicture}
\caption{Various effects of adjusting the \(a\) value in the equation \(y=a(2)^x\). Note how the general shape of the curve stays the same, but the y-intercept is moved depending on \(a\).}
\label{fig:1}
\end{figure}

\subsection{Base}
\label{sec:org0c3792f}

The base determines how quickly the dependent value will grow or decay. The base can be any number, but it must be \textbf{\textbf{greater}} than zero, and cannot equal 1. If \(b>1\), the graph will increase faster and faster as \(b\) increases. This appears as a steeper, rising graph. If \(0<b<1\), the graph will decrease faster as \(b\) decreases. This appears as a steeper, decreasing graph. See Fig. \ref{fig:2}. While there's nothing mathematically wrong with \(b=0\), it isn't really an exponential function at that point --- that falls into the category of \emph{constant} functions.

\begin{figure}
\centering
\label{fig:2}
\begin{tikzpicture}
\begin{axis}[
width=10cm,
clip=false,
legend pos=outer north east,
axis lines = center,
xlabel=\(x\),
ylabel=\(y\),
]
\addplot[thick,<->,
domain=-2:2,
samples=400,
color=red,
thick,
] {2^x};
\addlegendentry{\(y=2^x\)};
\addplot[thick,<->,
domain=-2:2,
samples=300,
color=blue,
] {4^x};
\addlegendentry{\(y=5^x\)};
\addplot[thick,<->,
domain=-2:2,
samples=300,
color=green,
] {(1/3)^x};
\addlegendentry{\(y=\frac{1}{3}^x\)};
\addplot[thick,<->,
domain=-2:2,
samples=300,
color=magenta,
] {(1/5)^x};
\addlegendentry{\(y=\frac{1}{5}^x\)};
\end{axis}
\end{tikzpicture}
\caption{Various exponential equations of the form \(y=b^x\) plotted. They all have a y-intercept of \((0,1)\) because of the exponent property \(b^0 = 1\).}
\end{figure}

\begin{mdframed}[style=tip,frametitle=Keyword: Asymptotes]
In both graphs shown previously, note how the curves never have an \(x\)-intercept. This is because the function can never be zero, hence it never actually touches/crosses the \(x\)-axis, though it gets very close. This type of curve is known as \emph{asymptotic}. Exponential functions have horizontal asymptotes at \(y=0\), which they approach but never touch. Later in this document is is shown that logarithmic functions exhibit similar asymptotic behaviour, but along the y-axis instead.
\end{mdframed}

\subsection{Features of the Exponential Graph}
\label{sec:orgd867268}

The exponential function \(y = b^x\), \(b>0\), \(b\neq 1\) has the following features:

\begin{itemize}
\item Injective (every \(x\) in domain has one and only one corresponding \(y\) in range.)
\item A horizontal asymptote at \(y = 0\), but no vertical asymptote.
\item Domain: \((-\infty, \infty)\).
\item Range: \((0,\infty)\)
\item End behaviour: As \(x \to \infty\), \(y \to \infty\). As \(x \to -\infty\), \(y \to 0\).
\item \(x\) intercepts: None
\item \(y\) intercepts: \((0,1)\).
\item Increasing if \(b>1\), decreasing if \(b<1\).
\end{itemize}

\section{Euler's Number}
\label{sec:org4082395}

Euler's Number\footnote{Euler is pronounced like ``Oiler''.} is an irrational number often used in exponential modeling.

\[e \approx 2.718281828.\]

LINKED: \href{https://www.youtube.com/watch?v=AuA2EAgAegE}{e --- Euler's Number (Numberphile)}

\section{Exponential Growth and Decay}
\label{sec:orgc31b103}

If the exponent is positive, then the function \emph{grows}.

\begin{mdframed}[style=equation,frametitle=Continuous Growth Formula]
\[y=ae^{nt}.\]

Where \(a\) is initial amount, \(n\) is rate of growth and \(t\) is time.
\end{mdframed}

If the exponent is negative in an exponential function, then the function shall \emph{decay}, as shown in Fig. \ref{fig:3}.

\begin{mdframed}[style=equation,frametitle=Continuous Decay Formula]
\[y=ae^{-nt}.\]
\end{mdframed}

\begin{figure}
\centering
\begin{tikzpicture}
\begin{axis}[
width=10cm,
clip=false,
legend pos=outer north east,
axis lines = center,
xlabel=\(x\),
ylabel=\(y\),
]
\addplot[thick,<->,
domain=-2:2,
samples=300,
color=red,
thick,
] {e^-x};
\addlegendentry{\(e^{-x}\)};
\addplot[<->,
domain=-2:2,
samples=300,
color=blue,
thick,
] {e^x};
\addlegendentry{\(y=e^x\)};
\end{axis}
\end{tikzpicture}
\caption{The functions \(e^x\) and \(e^{-x}\) are plotted.}
\label{fig:3}
\end{figure}

\section{The Logarithmic Function}
\label{sec:org7f6161f}

Logarithms are another way to write the same exponent. Logarithm form and exponential form and convertible between each other. In fact, it is this convertibility that helps us to solve exponential and logarithmic equations.

\begin{mdframed}[style=equation,frametitle=Exponential to Logarithm Form]
\[y=b^x \to x=\log_b y.\]
\end{mdframed}

\begin{mdframed}[style=examples]
\begin{align*}
&8^{\frac{1}{2}} = 2 \to \log_8 2 = \frac{1}{2}.\\
&3^4 = 81 \to \log_3 81 = 4.\\
&\log_5 125 = 3 \to 5^3 = 125.
\end{align*}
\end{mdframed}

\textbf{Note:} The base must be a positive number other than 1 for an exponential equation, and the same is true for logarithms. \href{https://math.stackexchange.com/questions/413713/log-base-1-of-1}{(Why? See StackExchange.)}

\begin{figure}
\centering
\begin{minipage}{7.5cm}
\begin{tikzpicture}
\begin{axis}[
width=7.5cm,
clip=false,
legend pos=south east,
axis lines = center,
xlabel=\(x\),
ylabel=\(y\),
]
\addplot[thick,<->,
domain=0:2,
samples=300,
color=red,
thick,
] {ln(x)/ln(10)};
\addlegendentry{\(y=log x\)};
\end{axis}
\end{tikzpicture}
\end{minipage}
\begin{minipage}{7.5cm}
\begin{tikzpicture}
\begin{axis}[
width=7.5cm,
clip=false,
legend pos=south west,
axis lines = center,
xlabel=\(x\),
ylabel=\(y\),
]
\addplot[thick,<->,
domain=-2:2,
samples=300,
color=blue,
thick,
] {10^x};
\addlegendentry{\(y=10^x\)};
\end{axis}
\end{tikzpicture}
\end{minipage}
\caption{The graph of $y=log_{10} x$ and $y=10^x$, inverses of each other.}
\end{figure}

\begin{mdframed}[style=tip,frametitle=Antilogarithms]
The \emph{antilogarithm} of a particular logarithm is the number that has that logarithm. Antilog $= 10^x$ for base 10 or $e^x$ for base $e$. For example, the antilog in base 10 of 2 is 100, because $10^2 = 100$.
\end{mdframed}

\subsection{Base}
\label{sec:org38836cb}

The value of the base, \(b\), affects the shape of a logarithmic function's graph, as shown in Fig. \ref{fig:5}. When \(b>1\), the function increases as the independent variable increases. When \(0<b<1\), the function decreases as the independent variable increases.

\begin{figure}
\centering
\begin{minipage}{7.5cm}
\begin{tikzpicture}
\begin{axis}[
legend columns =2,
width=7.5cm,
clip=false,
legend pos= south east,
axis lines = center,
xlabel=\(x\),
ylabel=\(y\),
]
\addplot[thick,<->,
domain=0:2,
samples=300,
color=red,
] {ln(x)/ln(10)};
\addlegendentry{\(y=\log_{10} x\)};
\addplot[<->,
domain=0:2,
samples=300,
color=blue,
thick,
] {ln(x)/ln(2)};
\addlegendentry{\(y=\log_2 x\)};
\addplot[<->,
thick,
domain=0:2,
samples=300,
color=green,
] {ln(x)/ln(1/2)};
\addlegendentry{$y=\log_{\frac{1}{2}}x$};
\addplot[<->,
thick,
color=brown,
domain=0:2,
samples=300,
] {ln(x)/(ln(1/4))};
\addlegendentry{$y=\log_{\frac{1}{4}}x$}
\end{axis}
\end{tikzpicture}
\end{minipage}
\begin{minipage}{7.5cm}
\begin{tikzpicture}
\begin{axis}[
width=7.5cm,
clip=false,
legend pos=south east,
axis lines = center,
xlabel=\(x\),
ylabel=\(y\),
]
\addplot[thick,<->,
domain=0:2,
samples=300,
color=red,
] {2 * ln(x)/ln(10))};
\addlegendentry{\(y=2\log_{10} x\)};
\addplot[<->,
domain=0:2,
samples=300,
color=blue,
thick,
] {5 * ln(x)/ln(10)};
\addlegendentry{\(y=5\log_{10} x\)};
\addplot[<->,
thick,
domain=0:2,
samples=300,
color=green,
] {((ln(x)/ln(10))/2};
\addlegendentry{$y=\frac{1}{2}\log_{10}x$};
\end{axis}
\end{tikzpicture}
\end{minipage}
\caption{Effects of various choices of base (left) and $a$ (right).}
\label{fig:5}
\end{figure}

\subsection{Stretching Factor}
\label{sec:orgd973011}

An exponential equation can be written like \(y=ab^x\), where \(a\) multiplies the output value of the parent function. This can be done with a logarithmic function, as well. With \(y=a \log_b x\), the \(a\) value gives a vertical stretch/compression factor. If \(|a|>1\), there is a vertical stretch away from the \(x\) axis --- the dependent variable is multiplied by \(a\). If \(0<|a|<1\), there is a vertical compression towards the \(x\) axis. The \(a\) value can also be negative, in which case the graph is reflected across the \(x\) axis. See Fig. \ref{fig:5}.

\subsection{Features of the Logarithm Graph}
\label{sec:orgc075db1}

The logarithmic function \(y=log_b x\), \(b>0\), \(b\neq 1\) has the following characteristics:

\begin{itemize}
\item Injective (one-to-one).
\item Vertical asymptote at \(x = 0\), but no horizontal asymptote.
\item Domain: \((0,\infty)\).
\item Range: \((-\infty,\infty)\).
\item End behaviour: As \(x \to \infty\), \(y \to \infty\). As \(x \to -\infty\), \(y \to 0\).
\item \(x\) intercepts: \((1,0)\).
\item \(y\) intercepts: None.
\item Increasing if \(b>1\), decreasing if \(0<b<1\).
\end{itemize}

\subsection{Evaluating Logarithms}
\label{sec:orgd52976c}

Most calculators only have base 10 and base \(e\), but there are a few ways to evaluate logarithms.

In some cases, you can convert to exponential form and find an answer that way.

\begin{mdframed}[style=examples]
\begin{align*}
\log_x 16 &= 2 \to x^2 = 16.\\
\therefore x &= 4.\\
\log_{\frac{1}{3}} x &= -4 \to \left(\frac{1}{3}\right)^{-4} = x.\\
\therefore x &= 3^4.
\end{align*}
\end{mdframed}

You can also use the \textbf{base change formula} to evaluate with a calculator:

\begin{mdframed}[style=equation]
\[\log_b a = \dfrac{\log_c a}{\log_c b}.\]

Where \(c\) is any other base you want to use.
\end{mdframed}

\begin{mdframed}[style=tip,frametitle=Log Bases]
The most frequently used logarithm bases are base 10, called the common log, and base \(e\), called the natural log. Base 2 is also used a lot in computer science. In most cases, the common log is just written as \(\log\), with the base implied as 10. The natural log is usually written as \(\ln\).
\end{mdframed}

\subsection{Log Rules}
\label{sec:org24c4b91}

\begin{mdframed}[style=equation]
    \begin{align*}
&\text{Product Rule: }\log_b(mn) \equiv \log_b(m) + \log_b(n).\\
&\text{Quotient Rule: }\log_b\left(\frac{m}{n}\right) \equiv \log_b(m) - \log_b(n).\\
&\text{Power Rule: }\log_b(m^n) \equiv n \cdot \log_b m.\\
&\text{Base Swap: }\log_b(m) \equiv \dfrac{1}{\log_m(b)}.\\
&\text{Base Change: }\log_b a \equiv \dfrac{\log_c a}{\log_c b}.\\
&\text{Log of the Base: }\log_b(b) \equiv 1.\\
&\text{Log of the Base to a Power: }\log_b(b^n) = n.\\
&\text{Log of Unity: }\log_b(1) \equiv 0.\\
&\text{Log of 0 or Negative Number: Undefined over $\mathbb{R}$.}\\
    \end{align*}
\end{mdframed}

\section{Other Formulas}
\label{sec:org1530bbe}
\label{sec:formulas}

\begin{mdframed}[style=equation]
\begin{align*}
&\text{Decibels of a sound } = 10 \log \dfrac{I}{I_o}.\\
&\text{Where $I$ is sound intensity and $I_o$ is the threshold of hearing for the average human ear.}\\
&\text{pH } = - \log[\text{H}^+].\\
&\text{Where H$^+$ is the Hydrogen ion concentration of the substance.}\\
&\text{Richter scale } = \log\dfrac{I}{I_o}.\\
&\text{Where $I$ is intensity and $I_o$ the reference intensity.}\\
&\text{Half-life decay } = I\left(\dfrac{1}{2}\right)^{\frac{t}{H}}.\\
&\text{Where $I$ is initial quantity, $t$ is time and $H$ is the half-life of the quantity.}\\
&\text{Simple interest } = Prt.\\
&\text{Where $P$ is principle, $r$ is rate and $t$ is time.}
\end{align*}
\end{mdframed}
\section{Calculus}
\label{sec:orgb43f4cc}

\begin{mdframed}[style=equation]
    \begin{align*}
&\od{}{x}(e^x) = e^x.\\
&\lim_{x\to \infty}\log_b(x) = \infty.\\
&\od{}{x}(\ln (x)) = \dfrac{1}{x}.\\
&\od{}{x}(\log_b(x)) = \dfrac{1}{x\ln(b)}.\\
    \end{align*}
\end{mdframed}

\section{Worked Solutions}
\label{sec:org0cde6ac}

\subsection{Conversion Between Forms}
\label{sec:org3a83bf8}

\emph{Convert \(\log_8 \dfrac{\sqrt{2}}{3}\) to exponential form.}

\begin{proof}
The exponential form is $y=b^x$, and its corresponding log form is $\log_b y = x$. In this case, set the expression equal to an unknown $x$.

\[\log_8 \dfrac{\sqrt{2}}{3} = x.\]

Then rearrange to correct form.

\[8^x = \dfrac{\sqrt{2}}{3}.\]
\end{proof}

\emph{Convert \(3^2 = 9\) to log form.}

\begin{proof}
An exponential equation of the form $y=b^x$ is convertible to log form as $x = \log_b y$.

In this case, $b=3$, $x=2$ and $y=9$.

Therefore, the log form of this equation is:

\[\log_3 9 = 2.\]

\end{proof}

\subsection{Log Rules}
\label{sec:orge3d6c71}

\emph{Use the logarithm rules to rewrite \(\log_5 \dfrac{25x^7y^8}{z^8}\).}

\begin{proof}

Quotient rule.

\[\log_5 (25x^7y^8) - \log_5 z^8.\]

Product rule.

\[\log_5 25 + \log_5 x^7 + \log_5 y^8 - \log_5 z^8.\]

Power rule. Note that $25 = 5^2$.

\[2 \log_5 5 + 7\log_5 x + 8 \log_5 y - 8\log_5 z.\]

Logarithm of a base.

\[2+7\log_5 x + 8 \log_5 y - 8\log_5 z.\]

\end{proof}

\emph{Solve for y in terms of x: \(\log_5 y = \log_5 x - \log_5 11 + \log_5 4\).}

\begin{proof}
What is requested is to solve $y$ in a way that depends on $x$. For the purposes of this problem, it is really needed to convert this to exponential form so as to get $y$ and $x$ out of the logarithms. The way to do this is to collect the logs on the right-hand side into one logarithm.

It can be tricky to correction collect the logarithms. The reader be warned that $-\log_5 11 + \log_5 4 \neq -\log_5(11\cdot 5)$. It \emph{is} however equal to $\dfrac{\log_5 4}{\log_5 11}$, simply because it can be rearranged as $\log_5 4 - \log_5 11$. Another way to look at it is to rewrite $-\log_5 11$ as $+\log_5 11^{-1}$, which is equivalent to $\log_5 \dfrac{1}{11}$.

However you implement the quotient rule, this problem will see $\log_5 11$ end up as a denominator.

\[\log_5 y = \log_5 \dfrac{x}{11} + \log_5 4.\]

Then use the product rule.

\[\log_5 y = \log_5 \dfrac{4x}{11}.\]

Convert to exponential form.

\[y = \dfrac{4x}{11}.\]

Which is the solution.

\end{proof}

\emph{Express as a single logarithm: \(\dfrac{1}{2}\log_b x + 2 \log_b y - 2 \log_b x\).}

\begin{proof}
My tip is to use the power rule to give the log arguments their exponents back, but make a small change: with $-2\log_b x$, bring the negative sign with the exponent.

I.e.,

\[\log_b x^\frac{1}{2} + \log_b y^3 + \log_b x^{-2}.\]

Using the product rule, this becomes

\[\log_b\left(x^\frac{1}{2}y^2x^{-2}\right).\]

The familiar exponent law $x^{-n} = \dfrac{1}{x^n}$ kicks in,

\[\log_b\dfrac{x^\frac{1}{2} y^3}{x^2}.\]

Then you'd have to divide the exponents to get the answer.

\[\log_b\dfrac{y^3}{x^\frac{3}{2}}.\]

\end{proof}

\subsection{Solving Equations}
\label{sec:org9512866}

\emph{How much carbon-14 would be left after 7000 years if the initial amount was 20 \si{\kilogram}? Carbon-14 has a half-life of \textasciitilde{}5370 years.}

\begin{proof}

Plug into the half-life formula (given in \hyperref[sec:formulas]{Other Formulas}).

\[\therefore y=20\, \si{\kilogram} \left(\dfrac{1}{2}\right)^{\frac{7000}{5730}}\approx 8.576\, \si{\kilogram}.\]

\end{proof}

\emph{Using the answer from the previous solution, go the other way: Find out what the value of the exponent was.}

\begin{proof}
\[8.576\, \si{\kilogram} = 20\, \si{\kilogram} \left(\dfrac{1}{2}\right)^{x}.\]

If you take the base 10 logarithm of both sides, you'll find you can apply the power rule, the product rule, and the quotient rule.

Power rule: \(\log(x^n)=n \log x\).

Product rule: \(\log(ab) = \log(a)+\log(b).\)

Quotient rule: \(\log(\frac{a}{b})=\log(a)-log(b).\)

\[\log\left(20\cdot \frac{1}{2}^x\right)=\log (8.576) \implies \log(20)+(-x \log(2))=\log(8.576).\]

Note: Since the common log of 1 is 0, \(\log\frac{1}{2}\) just ends up being \(-\log2.\)

At this point you really only need basic algebra to solve for x.

\[x= \dfrac{\log(8.576)-\log(20)}{-\log(2)} \approx 1.2216.\]

This is an excellent approximation to the actual value of x, as it is spot on to four decimal places. The only reason it is not precise was because I started with a rounded off value for y, and that trickles down throughout the process.
\end{proof}

\emph{Solve for \(D\): \(\ln D = \ln a -br +cr^2\).}

\begin{proof}

This is where being able to ``go the other way" with log rules is useful. Subtract $\ln a$ from both sides to get

\[\ln D - \ln a = -br+cr^2 \to \ln\dfrac{D}{a} = -br+cr^2.\]

Then change to exponential form. On the left-hand side, the logarithm goes away. The right hand side becomes an exponent with $e$ as the base.

\[\dfrac{D}{a} = e^{cr^2-br}.\]

Multiply both sides by $a$ to isolate $D$.

\[\therefore D = ae^{cr^2-br}.\]
\end{proof}

\emph{Solve for \(x\) in the equation \(0.5^x = 2^{x^2}\).}

\begin{proof}
Start by converting to log form, and applying the product rule.

\[x \log 0.5 = x^2 \log 2.\]

The best thing to to here is get the unknowns all on one side, so subtract $x\log 0.5$ from both sides.

\[x^2 \log 2 - x \log 0.5 = 0.\]

It is not necessary, but you could apply the quotient rule, too.

\[x^2 \log 2 - x (\log 1 - \log 2) = 0.\]

Note that $\log 1 = 0$, so it can be dropped.

\[x^2 \log 2 - x (- \log 2) = 0.\]

You can factor an $x$ out.

\[x(x \log 2 -(-\log 2) = 0 \to x(x\log 2 + \log 2) = 0.\]

Solve the factors.

\[x = 0 \text{ (Easy!)}\]

\[x\log 2 + \log 2 = 0 \implies x = - \dfrac{\log 2}{\log 2} = -1.\]

\[\therefore x = 0, -1.\]
\end{proof}

\emph{Rearrange for w in \(v = u \ln\dfrac{w_0}{w}\).}

\begin{proof}
Start by dividing both sides by $u$.

\[\ln\dfrac{w_0}{w} = \dfrac{v}{u}.\]

Method 1:

Change to exponential form now.

\[\dfrac{w_0}{w} = e^{\frac{v}{u}}.\]

Multiply both sides by $w$, then divide both sides by $e^{\frac{v}{u}}$.

\[w_0 = w\cdot e^{\frac{v}{u}} \to \dfrac{w_0}{e^{\frac{v}{u}}} = w.\]

Method 2:

Using the quotient rule, rewrite $\ln\dfrac{w_0}{w} = \dfrac{v}{u}$.

\[\ln w_0 - \ln w = \dfrac{v}{u}.\]

Add $\ln w$ to both sides, and subtract $\dfrac{v}{u}$ from both sides.

\[\ln w = \ln w_0 - \dfrac{v}{u}.\]

Convert to exponential form. This is a tricky area, because it looks like the right-hand side has two separate terms. However, the negative sign for the fraction will go with the exponent.

\[w = w_0\cdot e^{-\frac{v}{u}}.\]

(Don't believe it? Convert the above equation to logarithmic form.)

The negative exponent means this is an equivalent answer.

\[w = w_0\cdot e^{-\frac{v}{u}} = \dfrac{w_0}{e^{\frac{v}{u}}}\]

\end{proof}

/Solve \(x = 5^{\log_5 10}\).

Since the base on the right-hand side is the number 5, use the base 5 log on both sides.

\[log_5 x = log_5 10.\]

As the bases are equal, that means \(x=10\).

\emph{Solve for i in the electronics equation, \(11t = -\ln\left(1-\dfrac{i}{0.4}\right)\).}

\begin{proof}

The solution requires isolating the logarithm on the right hand side so that the equation can be converted to exponential form. When dealing with problems like this, it's essential that the logarithm be on its own so the conversion can be done. One way to accomplish this is to simply multiply both sides by $-1$.

\[-11t = \ln\left(1-\dfrac{i}{0.4}\right).\]

Then convert to exponential form.

\[1-\dfrac{i}{0.4} = e^{-11t}.\]

Subtract 1 from both sides.

\[-\dfrac{i}{0.4} = e^{-11t}-1.\]

Multiply both sides by $-0.4$.

\[\therefore i = -0.4(e^{-11t}-1) = -0.4e^{-11t}+0.4 = -\dfrac{0.4}{e^{11t}} +0.4.\]

Another option is, instead of multiplying both sides by $-1$ at the start, you can use the product rule to rewrite $-\ln\left(1-\dfrac{i}{0.4}\right)$ as $\ln\left(1^{-1}-\left(\dfrac{i}{0.4}\right)^{-1}\right).$ Such a path would proceed as follows.

Apply the negative exponents (remember $x^{-1} = \dfrac{1}{x})$.

\[\ln\left(1^{-1}-\left(\dfrac{i}{0.4}\right)^{-1}\right) = \ln\left(1-\dfrac{0.4}{i}\right).\]

Back in the main equation, convert to exponential form.

\[11t = \ln\left(1-\dfrac{0.4}{i}\right) \to 1-\dfrac{0.4}{i} = e^{11t}.\]

Subtract 1 from both sides.

\[-\dfrac{0.4}{i} = e^{11t}-1.\]

Note that $i$ is in the denominator, which makes it a little trickier to get out. Normally when I encounter that, I take the reciprocal of both sides.

\[-\dfrac{i}{0.4} = e^{-11t} - 1.\]

The rest of the solution is now the same as the first method demonstrated.

\end{proof}
\end{document}
