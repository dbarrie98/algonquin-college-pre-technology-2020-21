% Created 2021-04-19 Mon 13:37
% Intended LaTeX compiler: xelatex
\documentclass[10pt,letterpaper]{article}
               \usepackage{pgfplots}
               \usepackage{microtype,tikz,fancyhdr,amsthm}
\usepackage[framemethod=tikz]{mdframed}
\mdfdefinestyle{definition}{linecolor=black,frametitle=Definition,backgroundcolor=gray!20,roundcorner=10pt,linewidth=1.5pt,frametitlerule=true,skipabove=5pt,skipbelow=5pt,rightmargin=15pt,leftmargin=15pt}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{geometry}
\geometry{bottom=25mm}
\pagestyle{fancy}
\fancyhead[L]{\leftmark}
\fancyhead[R]{\thepage}
\fancyfoot[C]{}
\pgfplotsset{compat=1.17}
\usepgfplotslibrary{statistics}
\author{Devyn Barrie\thanks{barr0554@algonquinlive.com}}
\date{\today}
\title{Intro to Statistics\\\medskip
\large Professor Calvin Climie, April 12 class}
\hypersetup{
 pdfauthor={Devyn Barrie},
 pdftitle={Intro to Statistics},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.5)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents

\mdfdefinestyle{equation}{hidealllines=true,leftline=true,rightline=true,linecolor=blue,linewidth=1.5pt,skipabove=5pt,skipbelow=5pt,rightmargin=15pt,leftmargin=15pt}

\section{Frequency Distributions}
\label{sec:org7b4e534}

\subsection{Vocabulary}
\label{sec:org488d9e2}
\begin{itemize}
\item A \textbf{\textbf{dataset}} is a collection of data.
\begin{itemize}
\item A \textbf{\textbf{population}} refers to all elements of a particular group. For example, all college students in Canada is a population. A \textbf{\textbf{sample}} is a subset of a population taken for study. For example, the students at one college in Canada would be a sample of the population.
\end{itemize}
\item Raw data can be collected --- ``raw'' means the set is unorganized.
\item \textbf{\textbf{Array}}: Data that has been organized in some kind of order.
\item \textbf{\textbf{Classes}}: Groups used to organize data.
\item \textbf{\textbf{Histogram}}: Similar to a bar graph, except the bars are jammed together (see Fig. 1). Used to show frequency distribution. You can also use a frequency polygon, which has connected lines instead of bars.
\end{itemize}

\begin{mdframed}[style=equation]
\[\text{Mean} = \dfrac{\sum x}{n} = \dfrac{\text{all numbers added up}}{\text{number of numbers}}.\]
\end{mdframed}

\begin{itemize}
\item The \(\sum\) symbol is called ``\textbf{\textbf{sigma}}'' and is a math notation that means ``add everything up''. In statistics, the mean gets a special symbol: \(\overline{x}\).
\item \textbf{\textbf{Mean}} is one type of measure of central tendency.
\item Another is \textbf{\textbf{median}}. If you sort a numerical dataset in order of size (either highest to lowest or vice-versa), the number in the middle of the list is the \emph{median}. Unlike the mean, the median is not skewed by outliers in the data.
\begin{itemize}
\item If you have an even number of entries in the dataset, the median will be the mean of the middle two numbers.
\end{itemize}
\item The \textbf{\textbf{mode}} is the number that appears most often in a dataset.
\item \textbf{\textbf{Outliers}} are data values that differ significantly from the other observed values in a dataset.
\item \textbf{\textbf{Range}}: The difference between the highest number and smallest number in a dataset. The range tells you the spread of a dataset.
\item A frequency table is used to split up raw data into data classes. Each class groups data according to an interval. Each class gets a number that indicates how many times a piece of data in that class appears in the overall set.
\item To determine your classes, take the range of your data and divide by the number of classes you wish to have. The result is the class interval. Take the result and add to just below your lowest number.  Now start your next class one number above the previous class’ upper limit and add that amount to it, and so on. In class we found the range was 57, and divided by 6 to get 9.5. We then rounded up to 10 and that's why every interval was 10.
\item \textbf{\textbf{Relative Frequency}}: the ratio of frequency to total number of data in the set.
\item \textbf{\textbf{Percent Relative Frequency}} is the relative frequency converted to percent.
\end{itemize}

\begin{figure}
\centering
\begin{tikzpicture}
\begin{axis}[
title = {Percent of turbines by power rating},
ylabel = {$\%$ frequency},
xlabel = {MW},
ybar interval,
ytick distance = 2,
ytick = {0, 2,...,10},
yticklabels={0,8,16,24,32,40},
xticklabel =
\pgfmathprintnumber\tick--\pgfmathprintnumber\nexttick
]
\addplot+ [cyan!50!black,hist={bins=5, data min=2, data max = 7}]
    table [row sep=\\, y index=0] {
    data\\
    5.5\\ 4.1\\ 4.2\\ 6.5\\ 5.1\\ 2.4\\
    3.5\\ 4.7\\ 4.7\\ 3.5\\ 5.9\\ 6.6\\
    3.1\\ 5.1\\ 2.7\\ 4.4\\ 5.7\\ 4.4\\
    6.8\\ 5.9\\ 4.5\\ 5.5\\ 4.7\\ 4.9\\
    5.4\\
};
\end{axis}
\end{tikzpicture}
\caption{A percent frequency histogram of the frequency table given in Table 1.}
\end{figure}

\begin{table}[htbp]
\caption{Frequency table for Fig. 1.}
\centering
\begin{tabular}{lrrrrr}
Power Rating & 2-2.9 & 3-3.9 & 4-4.9 & 5-5.9 & 6-6.9\\
\hline
Frequency & 2 & 3 & 9 & 8 & 3\\
 &  &  &  &  & \\
\hline
 &  &  &  &  & \\
Relative Freq. & \(\frac{2}{25}\) & \(\frac{3}{25}\) & \(\frac{9}{25}\) & \(\frac{8}{25}\) & \(\frac{3}{25}\)\\
 &  &  &  &  & \\
\hline
 &  &  &  &  & \\
Percent Freq. & \(8\%\) & \(12\%\) & \(36\%\) & \(32\%\) & \(12\%\)\\
\end{tabular}
\end{table}

\section{Standard Deviation}
\label{sec:orgd5389b9}
\begin{itemize}
\item A special number that measures the amount of variation a dataset has. A small number indicates the values tend to be close to the mean, while a large number indicates the dataset is very spread out.
\end{itemize}

\begin{mdframed}[style=equation]
\[\sigma = \sqrt{\dfrac{\sum (x_i - \mu)^2}{N}}.\]

Where $x_i =$ each data value, $\mu =$ population mean and $N=$ population size.
  \end{mdframed}

\begin{itemize}
\item For samples, we use a modified formula.
\end{itemize}

\begin{mdframed}[style=equation]
\[s_x = \sqrt{\dfrac{\sum (x_i - \overline{x})^2}{n-1}}.\]

Where $n =$ sample size.
\end{mdframed}

\begin{itemize}
\item The difference with sample standard deviation is that we only have the sample mean, not \(\mu\). The practice of dividing by \(n-1\) is called \emph{Bessel's correction} and fixes the bias added by using sample mean instead of \(\mu\).
\item The formula can also be calculated on spreadsheet programs with the function \texttt{STDEV()} and \texttt{STDEV.S()}.
\end{itemize}

\section{Normal Distribution: The ``Bell Curve''}
\label{sec:orge2ad672}
\begin{itemize}
\item A distribution is an organized structure for arranging data.
\item A \emph{normal} distribution is one where a curve drawn over a histogram of the dataset has a bell-shape (see Fig. 2). The mean and median are equal. It is a rule that \(68.26\%\) of the data falls within one standard deiviation, \(95.44\%\) within two, and \(99.73\%\) within three.
\item Skewed distributions lean more to one side or the other. A distribution whose tail is on the right side is said to be positively skewed. If the tail is on the left side, we call it negatively skewed.
\end{itemize}

\begin{figure}
\pgfmathdeclarefunction{gauss}{3}{%
  \pgfmathparse{1/(#3*sqrt(2*pi))*exp(-((#1-#2)^2)/(2*#3^2))}%
}
\centering
\begin{tikzpicture}
\begin{axis}[
  no markers,
  domain=0:6,
  samples=100,
  ymin=0,
  axis lines*=left,
  xlabel=$x$,
  every axis y label/.style={at=(current axis.above origin),anchor=south},
  every axis x label/.style={at=(current axis.right of origin),anchor=west},
  height=5cm,
  width=12cm,
  xtick=\empty,
  ytick=\empty,
  enlargelimits=false,
  clip=false,
  axis on top,
  grid = major,
  hide y axis
  ]

 \addplot [very thick,cyan!50!black] {gauss(x, 3, 1)};

\pgfmathsetmacro\valueA{gauss(1,3,1)}
\pgfmathsetmacro\valueB{gauss(2,3,1)}
\draw [gray] (axis cs:1,0) -- (axis cs:1,\valueA)
    (axis cs:5,0) -- (axis cs:5,\valueA);
\draw [gray] (axis cs:2,0) -- (axis cs:2,\valueB)
    (axis cs:4,0) -- (axis cs:4,\valueB);
\draw [yshift=1.4cm, latex-latex](axis cs:2, 0) -- node [fill=white] {$68.26\%$} (axis cs:4, 0);
\draw [yshift=0.3cm, latex-latex](axis cs:1, 0) -- node [fill=white] {$95.44\%$} (axis cs:5, 0);

\node[below] at (axis cs:1, 0)  {$\mu - 2\sigma$};
\node[below] at (axis cs:2, 0)  {$\mu - \sigma$};
\node[below] at (axis cs:3, 0)  {$\mu$};
\node[below] at (axis cs:4, 0)  {$\mu + \sigma$};
\node[below] at (axis cs: 5, 0) {$\mu + 2\sigma$};
\end{axis}
\end{tikzpicture}
\caption{The normal distribution is characterized by a bell-shaped curve. (Adapted from \href{https://tex.stackexchange.com/a/100030}{TeX StackExchange)}}
\end{figure}
\end{document}
