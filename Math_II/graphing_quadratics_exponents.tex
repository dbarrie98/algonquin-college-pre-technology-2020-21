% Created 2021-03-22 Mon 15:38
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{microtype,tikz,float,pgfplots}
\date{\today}
\title{Graphing Quadratics + Exponents\\\medskip
\large Monday Math Class, Week 11 (Professor Calvin Climie)}
\hypersetup{
 pdfauthor={Devyn Barrie},
 pdftitle={Graphing Quadratics + Exponents},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.5)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents

\pagebreak

\section{Graphing Quadratics}
\label{sec:org4d829cd}

The shape of a quadratic equations's graph is called a parabola. The graph of a quadratic function in variable \(x\) either opens up or down. A parabola is a type of conic section.

\begin{figure}[H]
\centering
\begin{tikzpicture}
\begin{axis}[
axis x line = center,
axis y line = center,
title={\(f(x)=x^2\)},
xlabel=\(x\),
ylabel=\(f(x)\),
]
\addplot[
domain=-3:3,
samples=500,
color=red,
] {x^2};
\end{axis}
\end{tikzpicture}
\caption{A parabola that opens up.}
\end{figure}

\begin{figure}[H]
\centering
\begin{tikzpicture}
\begin{axis}[
axis lines=center,
title={\(f(x)=x^2\)},
xlabel=\(x\),
ylabel=\(f(x)\),
]
\addplot[
domain=-3:3,
samples=500,
color=red,
] {-x^2};

\end{axis}
\end{tikzpicture}
\caption{A parabola that opens down.}
\end{figure}

You need a minimum of three points to graph a reasonably accurate parabola. 

\begin{itemize}
\item Roots, found by factoring, completing the square, or with the quadratic formula \footnote{The formula is \(x=\dfrac{-b \pm \sqrt{b^2-4ac}}{2a}\).}. The Fundamental Theorem of Algebra guarantees two roots for all quadratics, so these count as two points even for a double root. (Of course, the roots aren't always real numbers. Plotting complex roots is not straightforward, but this \href{http://padi.psiedu.ubbcluj.ro/adn/article_8_1_5.pdf}{paper} discusses various methods.) 
\item The vertex, which consists of an ordered pair \((x,y)\), where \(x\) is the line of symmetry and \(y\) is the maximum or minimum of the function. You can get a decent sketch with just some calculated points in lieu of the roots, but you really can't get anywhere without knowing the vertex. Absolute must!
\end{itemize}

\section{Determining the Vertex}
\label{sec:org8028bed}

There are two ways to find the vertex. One is to complete the square. The other is to use a formula.

\subsection{Line of Symmetry Formula}
\label{sec:org45ed558}

The line of symmetry is an axis that runs parallel to the y-axis and cuts the parabola in half. It gives you the x-coordinate of the vertex, which you can use to find the y-coordinate by calculating the value of the equation at the x-coordinate.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			axis x line = center,
			axis y line = center,
			title={\(f(x)=x^2+2x+5\)},
			xlabel=\(x\),
			ylabel=\(f(x)\),
			]
			\addplot[
			domain=-5:3,
			samples=500,
			color=red,
			] {x^2 + 2*x + 5};
			\addplot[
			color=blue,
			] coordinates {(-1,-1) (-1,20)};
		\end{axis}
	\end{tikzpicture}
	\caption{In blue, the line of symmetry for a particular quadratic function.}
\end{figure}

\pagebreak

\[\text{Line of symmetry} = \dfrac{-b}{2a}.\]

\[\text{Vertex y-value} = f\left(-\dfrac{-b}{2a}\right).\]

Take \(f(x)=x^2+2x+5.\) The line of symmetry formula gives \(\dfrac{-2}{2}=-1\). Then \(f(-1)=4.\) Therefore, the vertex is \((-1,4)\).

\subsection{Completing the Square}
\label{sec:org8040bed}

This can be done on quadratics where the leading coefficient is 1. Then, you add and subtract the square of half the coefficient of x and use this formula:

\[x^2+bx+\left(\dfrac{b}{2}\right)^2-\left(\dfrac{b}{2}\right)^2 +c.\]

The first three terms will always form a perfect square trinomial that can be factored as \((x\pm\frac{b}{2})^2\).

Take for example the quadratic equation \(x^2+2x+5=0.\) It would then be written as \(x^2+2x+\left(\frac{2}{2}\right)^2-\left(\frac{2}{2}\right)^2+5 = 0.\)

Note: \(\left(\frac{2}{2}\right)^2 = 1.\)

Then finish up.

\[(x+1)^2 -1+5 = 0 \to (x+1)^2+4 = 0\]

This is called vertex form. The x-coordinate of the vertex is the number inside the brackets with opposite sign, while the y-coordinate is the constant outside the brackets. Hence, the vertex here is \((-1,4)\).

Completing the square works on any quadratic, but it can get messy sometimes. If the leading coefficient is greater than 1, you have to factor that number out of the expression before completing the square.

For example, \(4x^2+4x-2=0.\) Factor out 4 first to get \(4\left(x^2+x-\dfrac{1}{2}\right) = 0.\)

Completing the square:

\[4\left(x^2+x+\left(\dfrac{1}{2}\right)^2 -\left(\dfrac{1}{2}\right)^2-\dfrac{1}{2}\right)=0 \to 4\left(\left(x+\dfrac{1}{2}\right)^2 -\left(\dfrac{1}{2}\right)^2-\dfrac{1}{2}\right)=0\]

Since the 4 was factored out at the start, you have to distribute it at the end.

\[4\left(\left(x+\dfrac{1}{2}\right)^2 -\left(\dfrac{1}{2}\right)^2-\dfrac{1}{2}\right)=0 \to 4\left(x+\dfrac{1}{2}\right)^2 -1-2 =0\]

\[\therefore 4x^2+4x-2 = 4\left(x+\dfrac{1}{2}\right)^2 -3 =0. \]

The vertex of the quadratic is \(\left(-\dfrac{1}{2},-3\right)\).

In addition to finding the vertex, completing the square is useful because you can also get the roots from vertex form.

\[4\left(x+\dfrac{1}{2}\right)^2 -3 =0 \implies \left(x+\dfrac{1}{2}\right)^2 =\dfrac{3}{4} \implies x+\dfrac{1}{2} = \pm\sqrt{\dfrac{3}{4}} \implies x = -\dfrac{1}{2} \pm\sqrt{\dfrac{3}{4}}.\]


\subsection{Completing the Square vs Line of Symmetry Method}
\label{sec:org8041bed}

\begin{itemize}
	\item It can often be quicker to find the vertex using the line of symmetry method. Completing the square can be messy sometimes, and many people find it a tricky method.
	\item Completing the square also gets you close to finding the roots of the function. Then again, once you use the formula \(\dfrac{-b}{2a}\), you don't need to do much more work to get the whole quadratic formula.
	\item Completing the square is a simple way to get the general vertex form, \(a(x-h)^2+k\), which is used in finding the directrix and focus of parabolas.
\end{itemize}

\section{Laws of Exponents}
\label{sec:orge5b6c6b}

\begin{itemize}
\item Product Law: \(a^m \cdot a^n = a^{m+n}.\)
\item Quotient Law: \(\dfrac{a^m}{a^n} = a^{m-n}.\)
\item Power Law: \((a^m)^n = a^{mn}.\)
\item Zero exponent: \(a^0 = 1.\)
\item Negative exponent: \(a^{-m} = \dfrac{1}{a^m}.\)
\item One exponent: \(a^1 = a.\)
\end{itemize}

\section{Fractional Exponents}
\label{sec:org6b1e724}

Fractional exponents are used to represent roots.

\[\sqrt[n]{a} = a^{\frac{1}{n}}.\]

Additionally, the numerator can be larger than one, and represents two operations: the numerator is an exponent applied to the operand, and the denominator is the n-th root overall.

\[\sqrt[n]{a^m} = a^{\frac{m}{n}}.\]
\end{document}
