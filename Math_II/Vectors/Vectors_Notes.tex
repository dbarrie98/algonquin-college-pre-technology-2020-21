% Created 2021-04-04 Sun 20:30
% Intended LaTeX compiler: xelatex
\documentclass{article}
               \usepackage{pgfplots}
               \usepackage{microtype,tikz,fancyhdr,geometry,amsthm}
\usepackage[framemethod=tikz]{mdframed}
\mdfdefinestyle{definition}{linecolor=black,frametitle=Definition,backgroundcolor=gray!20,roundcorner=10pt,linewidth=1.5pt,frametitlerule=true,skipabove=5pt,skipbelow=5pt,rightmargin=15pt,leftmargin=15pt}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{tikz,microtype,float,amsthm,pgfplots,siunitx}
\renewenvironment{proof}{{\bf \emph{Solution.} }}{\hfill $\blacksquare$ \\}
\usepackage[style=chicago-authordate,backend=biber]{biblatex}
\addbibresource{~/Documents/Math/vector_citations.bib}
\pagestyle{fancy}
\fancyhead[L]{\leftmark}
\fancyhead[R]{\thepage}
\fancyfoot[C]{}
\geometry{bottom=30mm}
\colorlet{cyanish}{cyan!25!}
\author{Devyn Barrie\thanks{barr0554@algonquinlive.com}}
\date{\today}
\title{Intro to Vectors\\\medskip
\large January, 2021 (Original)}
\hypersetup{
 pdfauthor={Devyn Barrie},
 pdftitle={Intro to Vectors},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.5)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{What is a Vector?}
\label{sec:orgcbcc0cd}

\begin{itemize}
\item Many mathematical quantities just express a magnitude. Examples include time intervals, amounts of money, and lengths. Such things are called scalars.
\item A vector has a second component, direction. Only when you have described a magnitude and a direction do you have a vector.
\item Notation for a vector uses an arrow, such as \(\vec{v}\).
\item Examples of scalar quantities: speed, length, time, temperature.
\item Examples of vector quantities: velocity, weight (typically), force, acceleration.
\end{itemize}

\begin{center}
\includegraphics[scale=0.6]{/home/adidas/Documents/Math/vector.jpg}
\captionof{figure}{An example of a Vector (\cite{vec}).}
\end{center}

\section{Defining a Vector}
\label{sec:org2517055}

Vectors in 2D requires two pieces of information for a full description. One common way to do this is provide the magnitude (denoted \(||\vec{v}||\)) and an angle, \(\theta\). For example you could have a vector of 4 units magnitude with a direction given by 0.8 radians. Now you have a fully-described vector, baby!

\begin{figure}[H]
\centering
\begin{tikzpicture}[scale=2]
\draw[blue] (3mm,0mm) arc (0:30:4.85mm) node[midway,right]{\(\theta=0.8\)};
\draw[thick] (0,3) -- (0,0);
\foreach \x in {0,1,2,3}
    \draw (\x,2pt) -- (\x,-2pt); %ordered pair to define tick marks. the positive and negative pt tells it how far the tick should extrude on either side.
\foreach \y in {0,1,2,3}
    \draw (2pt,\y) -- (-2pt,\y);
\draw[thick] (0,0) -- (3,0) node[midway,above]{adj};
\draw[thick,red,->] (0,0) -- (3,3) node[midway,above,sloped]{\(||\vec{v}||=4\)};
\draw (3,3) -- (3,0) node[midway,right]{opp};
\end{tikzpicture}
\caption{A geometric representation of the vector described. If it were desired to find the x and y components of the vector magnitude, the triangle could be solved using the sine and tangent ratios. Can you do it?}
\end{figure}

\mdfdefinestyle{callout}{linecolor=black,backgroundcolor=cyanish,roundcorner=10pt,linewidth=1.5pt,frametitlerule=true,skipabove=5pt,skipbelow=5pt,rightmargin=15pt,leftmargin=15pt}

\begin{mdframed}[style=callout,frametitle=Equivalent Vectors]
If two vectors have the same magnitude and direction, they are considered equivalent.
\end{mdframed}

\section{Operations With Vectors}
\label{sec:org105ef00}
The simplest operation with vectors is addition. To find the sum of two vectors, their components of direction and magnitude must be considered. A vector sum is called a resultant. Vector addition is commutative, distributive and associative.

\subsection{Geometric Addition and Subtraction}
\label{sec:orgb56ea7c}

There are two ways to add vectors geometrically: the polygon method and the parallelogram method. With the polygon method, the resultant (i.e., the answer) is the ray drawn from the initial point of the first vector to the terminal point of the last vector. The resultant with the parallelogram method is the length of the diagonal.

\begin{figure}[H]
\centering
\begin{tikzpicture}
\draw[thick, ->] (0,0) -- (2,3);
\node at (1.3,1) {\(\vec{a}\)};
\draw[thick,->] (2,3) -- (-1,2);
\node at (0,2.8) {\(\vec{b}\)};
\draw[red,thick,->] (0,0) -- (-1,2);
\node at (-1,0.5) {\(\vec{a}+\vec{b}\)};
\end{tikzpicture}
\caption{An example showing how two vectors can be added using the polygon method. In this case the shape is not a right triangle, so you'd have to either measure the resultant side with a ruler or use trigonometry. Don't bother trying here, because I haven't given you enough information to solve the triangle.}
\end{figure}

When two vectors are involved and have different initial points, the polygon method is really just the triangle method. With more complicated polygons, you need to draw them to-scale so that you can outright measure the length of the resultant ray and arrive at an approximate answer. Yeah, not very rigorous. There is a more precise way to do it though!

\subsection{Adding and Subtracting the Components}
\label{sec:org81fca50}

A plane vector has an x and a y component. If you know both, you can just add them individually to the x and y components of any other vector.

E.g. if \(\vec{a}=(2,3)\) and \(\vec{b}=(1,4)\), then \(\vec{a}+\vec{b}=(2+1,3+4)=(3,7).\)

The typical way to decompose a vector into its x and y components is to treat the magnitude as the hypotenuse of a right triangle. Then, along with the direction \(\theta\), you can find the opposite and adjacent sides with the formulas,

\begin{align*}
\vec{v_x} &= \text{(magnitude)}\cos \theta.\\
\vec{v_y} &= \text{(magnitude)}\sin \theta.
\end{align*}

\textbf{Example: Find the x and y components of \(\vec{v}\) if the magnitude is 4 units at angle of 0.8 radians}

\begin{proof}

Using the triangle from fig. 2, we find that the opposite side can be solved as $4 \sin 0.8$.

That gives the adjacent as \(4\cos 0.8 \approx2.787.\)

\begin{figure}[H]
\centering
\begin{tikzpicture}[scale=2]
\draw[blue] (3mm,0mm) arc (0:30:4.85mm) node[midway,right]{\(\theta=0.8\)};
\draw[thick] (0,3) -- (0,0);
\foreach \x in {0,1,2,3}
    \draw (\x,2pt) -- (\x,-2pt); %ordered pair to define tick marks. the positive and negative pt tells it how far the tick should extrude on either side.
\foreach \y in {0,1,2,3}
    \draw (2pt,\y) -- (-2pt,\y);
\draw[thick] (0,0) -- (3,0) node[midway,above]{adj};
\draw[thick,red,->] (0,0) -- (3,3) node[midway,above,sloped]{\(||\vec{v}||=4\)};
\draw (3,3) -- (3,0) node[midway,right]{opp};
\end{tikzpicture}
\caption{\(\text{Opp} = 4\sin0.8\) and \(\text{adj} = 4 \cos 0.8.\)}
\end{figure}

\begin{align*}
\therefore \ &\vec{v_x}\approx 2.866. \\
&\vec{v_y}\approx 2.787.
\end{align*}

\end{proof}

Vector subtraction is just the opposite of addition. Consider it as adding a negative vector. A negative vector has the same magnitude but goes in the exact opposite direction.

\subsubsection{Geometric Example of a Negative Vector}
\label{sec:org0f31247}

\begin{figure}[H]
\centering
\begin{tikzpicture}
\draw[thick, ->] (0,0) -- (2,3) node[midway,above,left]{\(\vec{a}\)};
\draw[thick, ->] (2,3) -- (5,4) node[midway,above]{\(-\vec{b}\)};
\draw[red,thick, ->] (0,0) -- (5,4) node[midway,below,right]{\(\vec{a}+(-\vec{b})\)};
\end{tikzpicture}
\caption{The vector sum from fig. 3, except \(\vec{b}\) is now negative.}
\end{figure}

\subsubsection{Example of Subtracting Components}
\label{sec:org2049a07}

If \(\vec{a}=(2,3)\) and \(\vec{b}=(1,4)\), find \(\vec{a}-\vec{b}.\)

\begin{proof}

\[\vec{a}-\vec{b}=(2-1,3-4)=(1,-1).\]

By Pythagoras, the magnitude of the resultant vector is \(\sqrt{1^2+(-1)^2}=\sqrt{2}.\)

\end{proof}

\subsection{Finding The Resultant from Components}
\label{sec:org1b3e87f}

Use Pythagorean Theorem to find the resultant from components.

I.e.:

\[\vec{R} = \sqrt{\vec{v_x}^2 + \vec{v_y}^2}.\]

\subsection{Resultant Angle}
\label{sec:org2c7f353}

Finding the resultant angle is a process that begins with finding the resultant components. Since the components are nothing but the sines and cosines of the original vector's angle multiplied by magnitude, the signs of the resultant components will tell you which quadrant of the cartesian plane the resultant is in. Recall the ``All Students Take Calculus'' rule!

   \begin{figure}[H]
\centering
   \begin{tikzpicture}[scale=1.2]
   \draw[thick,fill,cyanish]
circle (3);
\draw
    (0,-3) -- (0,3);
\draw
    (-3,0) -- (3,0);
\node at (1,1){All};
\node at (-1,1){Students};
\node at (-1,-1){Take};
\node at (1,-1){Calculus};
\draw[thick, ->] (1,1.5) arc (0:180:1);
\draw[thick, ->] (-1,-1.5) arc (0:180:-1);
   \end{tikzpicture}
\caption{The mnemonic helps you recall what sign the functions have on the cartesian plane.}
   \end{figure}

In quadrant I, they are all positive. In quadrant II, sine is positive. In quadrant III, tangent is positive. In quadrant IV, cosine is positive.

\begin{mdframed}[style=callout,frametitle=Remember]
If the \(y\)-component is negative, that means sine is negative. If the \(x\)-component is negative, then cosine is too.
\end{mdframed}

\section{A Word on Scalar Multiplication}
\label{sec:org6bc85e3}

Scalars get their name from the fact that they \emph{scale} vectors. The product of a scalar and a vector is a vector. Multiplying a scalar with a vector changes the vector's magnitude. A positive scalar wouldn't change the direction, but a negative scalar would.

You can multiply the scalar through each of the components. If \(\vec{v}=(5,8)\), then \(2\vec{v}=(2\cdot 5, 2\cdot 8).\)

Scalar multiplication follows the same rules you are familiar with for the field of real numbers.

\begin{itemize}
\item Associative
\item Commutative
\item Distributive
\item The identity property \(1\vec{v}\equiv \vec{v}\)
\item The multiplicative property of -1 is the same.
\item The multiplicative property of 0 is the same.
\end{itemize}

\section{Worked Solutions}
\label{sec:org36c98c5}

\emph{A skydiver accelerates downward at \(9.8\) \si{\metre\per\second\squared}. There is a wind present on them, exerting an acceleration of \(2.2\) \si{\metre\per\second\squared} at an angle of \(\dfrac{13\pi}{180}\) above the horizontal. What is the resultant acceleration of the skydiver? Give the resultant angle as measured from the horizontal, in standard position, as well as measured from the vertical. Round to one decimal place.}\\

\begin{proof}
Decompose the magnitude into components. Let \(\vec{S} =\) the downward gravitational force on the skydiver, and \(\vec{W} =\) the upward force from the wind. We are given that,

\begin{align*}
\vec{S} &=9.8 \text{, } \theta = \dfrac{3\pi}{2}\\
\vec{W} &= 2.2 \text{, } \theta = \dfrac{13\pi}{180}
\end{align*}

The components are given by,

\begin{align*}
\vec{S_x} &= 9.8 \cos \dfrac{3\pi}{2} = 0\\
\vec{S_y} &= 9.8 \sin \dfrac{3\pi}{2} = -9.8\\
\vec{W_x} &= 2.2 \cos \dfrac{13\pi}{180} \approx 2.1436\\
\vec{W_y} &= 2.2 \sin \dfrac{13\pi}{180} \approx 0.4949
\end{align*}

Add the components.

\begin{align*}
\vec{R_x} &= 2.1436\\
\vec{R_y} &= 0.4949-9.8 = -9.3051
\end{align*}

Find the resultant magnitude.

\[||\vec{R}|| = \sqrt{2.1436^2 + (-9.3051)^2} \approx 9.5488.\]

Since \(\vec{R_x}\) is positive and \(\vec{R_y}\) is negative, the resultant must be in quadrant IV.

\[\text{Standard position angle} = 2\pi - \arctan\left|\dfrac{-9.3051}{2.1436}\right| \approx 5.0.\]

\[\text{Angle from vertical} = \dfrac{\pi}{2}-\arctan\left|\dfrac{-9.3051}{2.1436}\right| \approx 0.2.\]


\end{proof}

\pagebreak

\printbibliography
\end{document}
