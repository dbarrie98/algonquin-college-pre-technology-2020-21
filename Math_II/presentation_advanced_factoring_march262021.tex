\documentclass{beamer}
\usetheme{default}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{tikz,pgfplots,listings,polynom,graphicx}
\usetikzlibrary{decorations.markings}
\title{Advanced Factoring Methods}
\author{Devyn Barrie \newline\url{barr0554@algonquinlive.com}}
\institute{Algonquin College}
\date{March 26, 2021}
\begin{document}
\begin{frame}[plain]
    \maketitle
\end{frame}
\begin{frame}{Introduction}
	\begin{itemize}
		\item Topic for today: factorizing more advanced polynomials.
		\item We know how to factor simple equations like \(x^2-1=0\). 
		\item We also know that you sometimes can't factor, so we have the quadratic formula: \(x=\dfrac{-b\pm (b^2-4ac)^\frac{1}{2}}{2a}.\)
		\pause
		\item But there's \textbf{way more out there} when it comes to this! Typically in precalculus/calculus, students learn about methods like:
		\begin{itemize}
			\item The Factor and Remainder Theorems.
			\item The Rational Root Theorem.
			\item Iterative techniques like Newton's Method.
		\end{itemize}
	\item Today we will look at Factor/Remainder Theorem, RRT and demonstrate a simple Python script to factor a tricky polynomial. 
	\end{itemize}
\end{frame}

\begin{frame}{Important Concepts}
	
	(Non-rigorous)
	
	\begin{block}{Roots}
		For any polynomial \(p(x)\) set equal to zero, the ``roots" or ``solutions" of the polynomial are the values of \(x\) that satisfy the equality. Roots may be real numbers or imaginary. If they are real numbers, the roots will be \(x\)-intercepts on the equation's graph.
		
		\[a_n x^n + a_{n-1}x^{n-1} + \dotsb + a_2 x^2 + a_1 x + a_0 = 0\]
	\end{block}
	
	\begin{block}{Fundamental Theorem of Algebra}
		All polynomial equations of degree \(n\) have \(n\) complex roots.
		
		E.g. quadratics (degree 2) \alert{always} have two roots, real or imaginary (both are complex numbers). Cubics have three, quartics four, etc.
	\end{block}
\end{frame}

\begin{frame}{Question}
	How would you find the roots to this equation?
	
	\[20x^{10}+5x^8 -15x^6+10x^5 = 0.\]
	
	We will come back to this, once we learn about some critical tools for solving it.
	
\end{frame}

\begin{frame}{The Rational Root Theorem}
	\begin{itemize}
		\item A useful result in algebra, the \alert{Rational Root Theorem} allows us to determine all possible \emph{rational} roots to a polynomial. (It does not help us find imaginary or irrational roots.)
		\item A rational root may be a fraction or an integer. 
		\item Let \(P\) be a polynomial with integer coefficients, and set equal to zero. If it has any rational roots, then the possible roots are given by the ratios between factors of the constant term and the factors of the leading coefficient.
		\pause
		\begin{example}
		The list of candidates for rational roots of \(1x^2+5x-4=0\) is \(\frac{\pm 1, \pm 2,\pm 4}{\pm 1}\). The factors of the constant term, \(-4\), are the numerator. The factors of the leading coefficient, \(1\), are the denominator. (Unfortunately this particular quadratic has no rational roots.)
		\end{example}
	\end{itemize}
\end{frame}

\begin{frame}{Factor and Remainder Theorem}
	\begin{itemize}
		\item \alert{Factor Theorem}: Let \(P\) be an arbitrary polynomial. If \(P(b)=0\), then \((x-b)\) is a factor of \(P(x)\).
		\pause
		\item So as long as we find a number that produces a zero, we know we have a factor. 
	\end{itemize}
	
	\begin{example}
		\(x^2+4x-5=0\) factors into \((x+5)(x-1)=0\). You can put \(-5\) or \(+1\) into the equation and get back a zero. The factors and roots have opposite signs. 
	\end{example}
\end{frame}

\begin{frame}{Factor and Remainder Theorem Cont.}
	\begin{itemize}
		\item Also useful to know: Remainder Theorem.
		\item If you divide \(P\) by \((x-r)\), where \(r\) is any constant, then the remainder shall be \(P(r)\).
		\pause
		\item This implies that if the remainder of \(P\) when divided by \((x-r)\) is zero, then \((r)\) must be a root of the polynomial.
		\pause
		\begin{example}
			Say we have \(2x^3+37x^2+132x-288=0\). There is no remainder after dividing by \((x+12)\). As well, using \(x=-12\) produces a zero, which is the rule. 
		\end{example}
	\end{itemize}
\end{frame}

\begin{frame}{Can We Solve It?}
	Now we can tackle this problem.
	
		\[20x^{10}+5x^8 -15x^6+10x^5 = 0.\]
		
		Step 1: Factor out \(5x^5\), since it is the greatest common factor.
		
		\pause
		
		\[20x^{10}+5x^8 -15x^6+10x^5 = 5x^5(4x^5+x^3-3x+2)=0.\]
		
		\pause 
		We already have 5 roots! The multiplicative property of 0 means that \(5x^5 = 0 \implies x=0\). Since any polynomial of \(n\) degree has \(n\) roots, that means \(5x^5\) counts for 5 roots. 
		
\end{frame}

\begin{frame}{Can We Solve It? Cont.}
	Step 2: List out the possible rational roots for the 5th degree polynomial inside the brackets. 
	
	\[5x^5(4x^5+x^3-3x+2)=0.\]
	
	\pause
	
	\[\dfrac{\pm 2, \pm 1}{\pm 4, \pm 2, \pm 1}.\]
	
	\pause
	
	Note there are many possibilities. Could be \(2, -2, 1, -1, \frac{1}{4}\), etc. There are methods to make the checking process a little easier, but I will not discuss them today. (I am namely thinking of upper/lower bounds and intermediate value theorem.)
	
\end{frame}

\begin{frame}{Find the Zero}
	\begin{itemize}
\item Step 3: If we try the possible values, we eventually will find that \(x=-1\) produces a zero. 
\item What does that tell us about a possible factor of the equation?
	\end{itemize}
	
	\pause
	
\begin{itemize}
	\item If you think it must mean \((x+1)\) is a factor, you'd be correct!
\end{itemize}
\end{frame}

\begin{frame}{Factor!}
	\begin{itemize}
		\item Step 4: Use polynomial long division to factor.
	\end{itemize}
\pause
\polylongdiv{4x^5+x^3-3x+2}{x+1}
\end{frame}

\begin{frame}{Finishing Up}
	\begin{itemize}
\item At this point, we are done. You can try another rational root test, but it will fail. As it happens, none of the remaining 4 roots are real numbers!
	\end{itemize}

\[20x^{10}+5x^8 -15x^6+10x^5 = 5x^5(x+1)(4x^4-4x^3+5x^2-5x+2)=0.\]

\[\therefore x = 0, -1.\]
\end{frame}

\begin{frame}{Factor Theorem vs. Quintic}
	\begin{itemize}
		\item Quintics are an interesting beast. They are 5th degree polynomials, meaning their highest power is 5.
		\item It is a very famous result of abstract algebra that no general solution exists for quintic equations in terms of radicals. They are possible to solve, but there is no formula like what we have for quadratics, cubics or quartics.
		\item This result came from work done by Évariste Galois, a French mathematician who lived more than 180 years ago. 
		\pause
	
	\item How can we use factor theorem to make a Python script to solve this equation?
		
		\[x^5 + 150 x^4 - 23 x^3 - 635232 x^2 - 7140560 x + 615264000=0\]
	
	\end{itemize}
\end{frame}

\begin{frame}[fragile]{Demonstration: Python Script}
\lstinputlisting[language=Python, breaklines=true]{bad_factorizer.py}

\begin{itemize}
	\item Import Numpy for math in Python.
	\item Enable \verb*|polyval| to evaluate polynomial at desired point.
	\item For loop checks all integers in given range to see if they produce a zero. 
\end{itemize}
\end{frame}
\begin{frame}[fragile]{So What Happens?}
	\begin{itemize}
		\item Remember, the factor theorem states that if \(P(b)=0\), then \((x-b)\) is a factor of \(P(x)\). 
	\end{itemize}

\alert{Let's try the program out...}

\pause
	
\begin{lstlisting}
	>>> %Run bad_factorizer.py
	-100
	-80
	-51
	29
	52
\end{lstlisting}
	The program has given us the integers for which \(P(b) = 0\) is true.
	
	Hence, the factored form of the polynomial is:
	
	\[(x+100)(x+80)(x+51)(x-29)(x-52)=0.\]
\end{frame}
\begin{frame}{Graph of the Quintic}
	\[f(x)=x^5 + 150 x^4 - 23 x^3 - 635232 x^2 - 7140560 x + 615264000\]
	
\begin{figure}[H]
	\begin{tikzpicture}
		\begin{axis}[
			axis lines = center,
			clip=false,
			xlabel=\(x\),
			ylabel=\(f(x)\),
			]
			\addplot[thick,<->,
			domain=-110:60,
			samples=500,
			color=red,
			] {x^5 + 150*x^4 - 23*x^3 - 635232*x^2 - 7140560*x + 615264000};
		\end{axis}
	\end{tikzpicture}
\end{figure}
\end{frame}
\begin{frame}
	\begin{itemize}
		\item That was a pretty bad script, though. There are established ways to get computers to factor polynomials for you, and this isn't it.
		\item  As far as computer algebra goes, this is as bad as it gets. I am told the computational complexity of this program is \(O(n!).\) Meaning, it gets significantly slower to work the wider I make the range. \pause
		\item For this polynomial, I know it will work because I made up the roots and then constructed the quintic, so I know the roots are in the range. 
		\item If I didn't know the range the roots are in, you'd need to use a really big range and even then couldn't be sure you'd find them all. \pause
		\item Basically, don't actually use this program!
\end{itemize}
\end{frame}

\begin{frame}{Wrapping Up}
	\begin{itemize}
		\item Today we covered some methods that can help factor higher-degree polynomials.
		\item Key ideas:
		\begin{itemize}
			\item Rational Roots Theorem: possible RRs are \(\frac{\text{factors of constant term}}{\text{factors of leading coefficient}}\).\\
			
			\item Factor Theorem: If \(P(b)=0\), then \((x-b)\) is a factor.
			\item Remainder Theorem: If division of \(P\) by \((x-b)\) produces a remainder of zero, then \(b\) is a root. \pause
			\item A for loop is not the best way to get computers to do it for you.
			
			\pause
			
			\item Thank you for attending!
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Check Your Understanding}
	\begin{enumerate}
		\item Fully factor \(2x^3+37x^2+132x-288=0.\)
		
		\item If \(P(b) = 1\), what does that tell us about \(b\)?
		
		\item Use the Rational Root Theorem to find the possible rational roots of \(6x^2+24\). Are any of them a true root?
		
		\item Use what you learned today to fully factor \(6n^3-18n^2-216n+648=0.\)
	\end{enumerate}
\end{frame}

\begin{frame}{Solutions}
	\begin{enumerate}
		\item Earlier in the presentation, a factor is given: \((x+12)\). Use polynomial long division to arrive at \((x+12)(2x^2+13x-24)=0\). Then factor the quadratic by grouping to finish: \((x+12)(2x-3)(x+8)=0\). \pause
		\item It tells us that \(b\) is not a root of \(P\). \pause
		\item \(\frac{\pm 1, \pm 24, \pm 12, \pm 8, \pm 6, \pm 4, \pm 2, \pm 3}{\pm 1, \pm 2, \pm 3, \pm 6}\). None are actually a root, as this equation is solvable only over the complex numbers; it factors into \(6(x+2i)(x-2i).\) \pause
		\item Start by factoring out a 6 to get \(6(n^3-3n^2-36n+108)=0\). Possible rational roots are \(\pm 1, \pm 108, \pm 2, \pm 54, \pm 27, \pm 36, \pm 18, \pm 12, \pm 6, \pm 4, \pm 9, \pm 3\). Trying all finds that 3 gives a zero. Then long-divide by \((n-3)\) to get \(6(n-3)(n^2-36)\). Then finish up by factoring the difference of squares. \(6(n-3)(n-6)(n+6)=0.\)
	\end{enumerate}
\end{frame}
\end{document}