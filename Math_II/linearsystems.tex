% Created 2021-01-26 Tue 21:57
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{fullpage,microtype,tikz,pgfplots,float,amsthm,amstext}
\renewenvironment{proof}{{\bf \emph{Solution.} }}{\hfill Bob's your uncle. \\}
\date{\today}
\title{Systems of Linear Equations}
\hypersetup{
 pdfauthor={Devyn Barrie},
 pdftitle={Systems of Linear Equations},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.5)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{Introduction}
\label{sec:orgd845d3c}

A system of linear equations is a set of two or more linear equations that are considered together. They have the same set of variables, and the solution to a system is one such that all equations in the system are simultaneously satisfied.

There are two main ways to solve a system. You can do it graphically or algebraically. A system of two linear equations in two real variables will have three cases for a solution:

\begin{enumerate}
\item No solution.
\item Infinite solutions.
\item One unique solution.
\end{enumerate}

\subsection{A Review of Linear Equation Form}
\label{sec:org0163c4e}

We will be using standard form in this document. This document assumes you already know how to work with linear equations in a general sense.

The form is:

\[ax+by=c.\]

\section{Graphical Solution}
\label{sec:org2fdcf76}

A system of two linear equations can be solved graphically. You graph them and see if the lines cross. The coordinate where the lines intersect is the solution.

\begin{figure}[H]
\centering
\begin{tikzpicture}[scale=1]
\begin{axis}[
axis lines=center,
xlabel=\(x\),
ylabel=\(y\),
 xlabel style={at={(axis description cs:1.03,0.5)},anchor=north},
    ylabel style={at={(axis description cs:0.5,-0.15)},anchor=south},
]
\addplot[
domain=-5:6,
samples=200,
color=red,
] {(-x)};
\addlegendentry{\(y=-x\)}
\addplot[
domain=-5:6,
samples=200,
color=blue,
] {-2*x+3};
\addlegendentry{\(y=-2x+3\)}
\end{axis}
\end{tikzpicture}
\caption{A system of linear equations, with solution at \((3,-3).\)}
\end{figure}

If the lines are parallel, they will never cross, so there is no solution.

If the lines overlap (in other words, if they are the same line) then you have infinite solutions.

But graphically solving these isn't very precise in most cases.

\section{Algebraic Solution}
\label{sec:orgccb6f5c}

There are two ways to solve a system with algebra.

\subsection{Substitution}
\label{sec:org6089574}

One way is to just solve for one variable, then drop in the expression that this provides into the original equation. You are then able to solve for the remaining variable.

Let's look at an example.

Solve the system that consists of:

\begin{equation}
4x+7y=18
\end{equation}

\begin{equation}
2x-y=4
\end{equation}



\begin{proof}
In the second equation, you can easily solve for y by subtracting \(2x\) from both sides and then multiplying both sides by -1.

\[-y=-2x+4 \implies y=2x-4.\]

We now know the value of y. In the first equation, we can replace y with \(2x-4.\)

\[4x+7(2x-4)=18.\]

Now it is just a linear equation of one variable. Distribute the 7 into the binomial, and carry on as usual.

\[4x+14x-28=18 \implies 18x-28=18 \implies 18x=18+28.\]

\[\therefore x = \dfrac{23}{9}.\]

Now go back to the value of y we found earlier. Drop in the value of x.

\[y=2\left(\dfrac{23}{9}\right)-4 \implies y=\dfrac{10}{9}.\]

Now with these values, let's check if the equations are satisfied.

Equation 1:

\[4 \left(\dfrac{23}{9}\right) +7 \left(\dfrac{10}{9}\right)=18.\]

It's true, so we're good there.

Equation 2:

\[2\left(\dfrac{23}{9}\right) -\dfrac{10}{9}=4.\]

That is also right!

The solution to the system is then written as an ordered pair.

\[\left(\frac{23}{9}, \frac{10}{9}\right)\]
\end{proof}

\subsection{Elimination}
\label{sec:org3870a16}

Let us try the same system except with elimination.

I am a big fan of this method because it is often quicker than substitution. Sometimes you have to be a little creative. The aim is to eliminate a variable from one of the equations, thus making it an equation of one variable. You can do anything you want to the equation as long as it is done to both sides. If you've done it right, you can then add the equations and eliminate your chosen variable.

\begin{proof}
I will multiply both sides of the second equation by 7.

\[7(2x-y)=7(4)\implies 14x-7y=28.\]

Now I can add both equations together, canceling the y variable because I will wind up adding 7 to -7.

To add the equations, simply add the like terms. The x's go with x, and y's go with y. You also add the right sides to each other. The result is one equation with one variable.

\[18x=32.\]

\[\therefore x=\frac{46}{18}=\frac{23}{9}.\]

Looks familiar!

You then take this value and drop it into any of the original equations. Then solve for y.

\[4\left(\frac{23}{9}\right)+7y=18 \implies 7y=18-\frac{4\cdot 23}{9}.\]

I want to be careful here, because if I'm not I'll get something ugly: \(y=\frac{18}{7}-\frac{4\cdot 23}{9}.\)

I don't think you want to deal with those fractions, so we can avoid some of that by multiplying both sides of what I got by 9. That eliminates the fraction on the right side.

\[9(7y)=9(18-\frac{4\cdot 23}{9}) \implies 63y = 162-4\cdot 23.\]

Nice! That knocks out the denominator of the fraction since the 9 cancels with 9, and leaves the numerator alone. The other numbers that weren't part of the fraction got multiplied by 9.

Now divide both sides by 63.

\[y=\frac{18-4\cdot 23}{63} = \frac{70}{63} = \frac{10}{9}.\]

Looks like we have our solutions! We can skip checking them because we already know they are correct.

\end{proof}

\section{Fun Math History: Diophantine Equations}
\label{sec:orgbc82524}

Diophantine equations were important in the early history of algebra. A Diophantine equation is an equation with integer coefficients, in two or perhaps more variables, and where only integer solutions are allowed. They are named after the ancient Alexandrian mathematician, Diophantus, who lived more than 1700 years ago.

Finding integer solutions can be challenging. Diophantine equations continued to be investigated by number theorists long after his death. Hilbert's 10th problem, proposed in 1900, asked if there exists a universal algorithm that can solve any Diophantine equation. While there is such an algorithm for linear Diophantine equations (it is called the Extended Euclidean Algorithm), it has been proven that there does not exist a general solution.

You can determine if a solution exists for a linear Diophantine equation of the form

\[ax+by=c\]

quite easily. Simply determine the greatest common divisor of a and b. If \(\text{gcd}(a,b)|c\), then a solution exists. (The | symbol is used in number theory as shorthand for ``divides''.)

If you'd like to read more about Diophantine equations, check out \href{https://brilliant.org/wiki/linear-diophantine-equations-one-equation/}{The Brilliant Wiki}.
\end{document}
