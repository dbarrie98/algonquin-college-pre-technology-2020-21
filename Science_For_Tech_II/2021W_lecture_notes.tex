% Created 2021-03-12 Fri 12:36
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{microtype}
\author{Devyn Barrie}
\date{\today}
\title{SCI5901-300 Lecture Notes\\\medskip
\large 2021W}
\hypersetup{
 pdfauthor={Devyn Barrie},
 pdftitle={SCI5901-300 Lecture Notes},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.5)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{Jan 15: The Atomic Nucleus and Radioactivity}
\label{sec:org2eefb80}

\subsection{Radioactivity}
\label{sec:orgdc193d3}

\begin{itemize}
\item Results from radioactive decay, which is the process where unstable atomic nuclei transform and emit radiation.
\item Unstable atomic nuclei have an imbalance of neutrons to protons, so they are radioactive.
\begin{itemize}
\item The strong nuclear force holds protons and neutrons together in the nucleus. We know it has a \emph{very} short range. If you don't have enough of both protons and neutrons in a nucleus, then they will not be attracted together enough by the strong nuclear force and will be repelled. This causes the nucleus to come apart, which is radioactive decay.
\end{itemize}
\item Neutrons help hold the nucleus of an atom together. Both protons and neutrons experience the strong nuclear force, but since protons repel each other and neutrons do not, you need more neutrons than protons to increase the strength of the force.
\begin{itemize}
\item Heavier atoms will have neutrons farther apart, so the force is weaker and this is why heavier elements emit more radiation.
\end{itemize}
\end{itemize}

\subsection{The abundance and hazards of radiation}
\label{sec:org41b230f}
\begin{itemize}
\item Most radiation we encounter is natural background radiation that originates from Earth and space (cosmic rays).
\begin{itemize}
\item This radiation gets more intense at higher altitudes.
\end{itemize}
\item The earth's magnetic field protects us from the worst of the bombardment.
\item The main reason why radiation can be dangerous is that it damages our cells. Luckily, we have evolved to withstand a little radiation because our cells can repair themselves. It can also cause cancer because it disrupts the regulation of our cells.
\item All matter emits radiation to some degree, but not always high-energy. This is why only some radioactive materials are actually dangerous.
\end{itemize}

\subsection{Cosmic rays}
\label{sec:org54f0c0a}
\begin{itemize}
\item There are two types:
\begin{itemize}
\item High energy particles, mostly protons.
\item High-frequency electromagnetic radiation (gamma rays), which affect us by transforming nitrogen atoms in the air to radioactive carbon-14, which winds up in plants.
\end{itemize}
\end{itemize}

\subsection{Types of radioactive decay}
\label{sec:orga6aa5a9}
\begin{itemize}
\item Alpha: Particle that carries a positive electrical charge.
\begin{itemize}
\item Consists of two protons and two neutrons (a helium nucleus). Usually emitted by heavy nuclei like uranium.
\item Loses energy quickly during interaction and low penetrating ability. However, it can cause significant damage to the surface of living tissue due to its high kinetic energy.
\item Will pick up electrons and turn into harmless helium when travelling through air.
\item Is deflected by magnetic or electric fields.
\end{itemize}
\item Beta: Particle carries a negative charge.
\begin{itemize}
\item An electron ejected from a neutron. The electron is produced in a reaction where a neutron actually turns into a proton plus an electron. This happens due to the neutron's natural instability, and it will decay. The proton stays put, and the electron flies off.
\item Has a smaller mass and electric charge than an alpha particle, and has higher kinetic energy.
\item Loses energy more slowly when moving through air, so travels farther.
\item Penetrates deep into skin, so more potential to cause harm to cell life.
\item Once stopped, it becomes an ordinary electron.
\end{itemize}
\item Gamma: Ultra high energy nonvisible light that carries no charge.
\begin{itemize}
\item Emitted when a nucleus in an excited state moves to a lower energy state.
\item The most harmful type of radiation. High-penetrating.
\item More energy per photon than in visible or ultraviolet light and x-rays. It has so much energy, if it was absorbed by an electron in an atom, it will knock the electron right off the atom. For this reason we call gamma rays an \textbf{ionizing} radiation. If this happened at a large scale inside your body, bad things happen.
\item Unaffected by magnetic and electrical fields.
\item Gamma radiation doesn't persist, so it does not cause things to become radioactive.
\end{itemize}
\end{itemize}

\subsection{Half-life and transmutation}
\label{sec:org8e428e5}

\begin{itemize}
\item The rate of decay for a radioactive isotope.
\item Half life is defined as the time required for half of a starting quantity of an element to decay.
\item Half-life is constant and independent for any physical or chemical change the atom may undergo. It can be calculated at any given moment by measuring the rate of decay of a known quantity using a radiation detector. Half-life equations are exponential functions that look like

\[y=I\left(\dfrac{1}{2}\right)^{\frac{t}{H}}.\]

Where
\begin{itemize}
\item \(I=\text{Initial quantity}\).

\item \(t=\text{Time}\).

\item \(H=\text{Half-life}\).
\end{itemize}
\end{itemize}

Example: How much carbon-14 would be left after 7000 years if the initial amount was 20kg?

Solution:

The half-life of carbon-14 is 5730 years.

\[\therefore y=20_{\text{kg}} \left(\dfrac{1}{2}\right)^{\frac{7000}{5730}}\approx 8.576_{\text{kg}}.\]

\pagebreak

BONUS: How would you go the other way? Let's say we had y but didn't know the value of the exponent.

\[8.576=20_{\text{kg}} \left(\dfrac{1}{2}\right)^{x}.\]

If you take the base 10 logarithm of both sides, you'll find you can apply the power rule, the product rule, and the quotient rule.

Power rule: \(\log(x^n)=n \log x\).

Product rule: \(\log(ab) = \log(a)+\log(b).\)

Quotient rule: \(\log(\frac{a}{b})=\log(a)-log(b).\)

\[\log(20\cdot \frac{1}{2}^x)=\log (8.576) \implies \log(20)+(-x \log(2))=\log(8.576).\]

\emph{Note: Since the common log of 1 is 0, \(\log\frac{1}{2}\) just ends up being \(-\log2.\)}

At this point you really only need basic algebra to solve for x.

\[
x= \dfrac{\log(8.576)-\log(20)}{-\log(2)} \approx 1.2216.
\]

This is an excellent approximation to the actual value of x, as it is spot on to four decimal places. The only reason it is not precise was because we started with a rounded off value for y, and that trickles down throughout the process.

\section{Jan 22: Radiation cont.}
\label{sec:orgd777a7d}

\subsection{Radiometric Dating}
\label{sec:org0cf374e}

\begin{itemize}
\item We can use carbon dating to determine the age of fossils.
\begin{itemize}
\item Based on the ratio of carbon-12 to carbon-14.
\item The half-life of carbon-14 is about 5760 years, which is short when you consider the age of the earth.
\item The method is not exact due to fluctuations in Earth's carbon-14 production.
\end{itemize}
\item Nonliving things can be dated through uranium and lead isotopes.
\begin{itemize}
\item Uranium-238 decays into lead-206. Uranium-235 decays into lead-207. Half lives are close to a billion years.
\end{itemize}
\end{itemize}

\subsection{Nuclear Fission}
\label{sec:orgafbf4fd}
\begin{itemize}
\item Nuclear fission is when a neutron with high kinetic energy splits an atom apart.
\item The only naturally-occurring atom where fission can readily occur is uranium-235.
\item You then get back, most probably, Krypton-91 and Barium-142, plus some free neutrons that fly off. These particular isotopes that come out of fission are very radioactive. We call them ``daughter nuclei''.
\begin{itemize}
\item Is it always those isotopes we get back? No. The protons need to add up to 92 for sure, but the specific shares of neutrons can change, so the exact daughter isotopes you get back are a matter of probability.
\end{itemize}
\item You also get free neutrons. ``You don't have to pay for the neutrons, they're totally free.'' (Missed opportunity to say they come at no charge, professor.)
\item These free neutrons that split off can go and cause more fission events to occur. This can create a chain nuclear reaction. Usually you get two or three free neutrons.
\item The energy released comes in the form of the kinetic energy of the fission products, including some energy for the ejected neutrons. Energy not released as kinetic is released as gamma rays.
\item There are two ways to cause a chain nuclear reaction:
\begin{itemize}
\item Get a critical mass, where you have the minimum amount of fissionable material that will sustain a chain reaction. If it's the right size, every neutron will cause a fission to occur. (This is how atom bombs are made, each fission causes more than one new fission to occur.)
\item Fission reactors spread out the uranium more and don't use a pure blob of it. A lot of it is U-238, which absorbs neutrons and does not fission. Reactors also have a moderator to slow the neutrons down so that they are less likely to have enough kinetic energy to split U-235 nuclei. In a reactor, each fission causes one more fission to occur, so that you get a steady rate of energy release. Reactors cannot explode like a bomb because they don't use pure U-235.
\item CANDU reactors can't even melt down. They use deuterium as a moderator. CANDU also uses approximately 98\% U-238. So everywhere you look, neutrons are being stopped in their tracks. ``It just barely works in the first place''. You can also refuel a CANDU reactor while it runs at full power.
\item One other advantage of CANDU reactors is that they don't require enriched uranium, which is costly and requires facilities that could be used for nuclear weapons proliferation.
\item One downside of CANDU reactors is that they actually produce fissionable plutonium from the U-238. In theory this plutonium could be used as fuel in a different reactor, but building such a plant is politically problematic.
\end{itemize}
\end{itemize}

\subsection{Uranium vs Plutonium}
\label{sec:org053dfec}
\begin{itemize}
\item Elements heavier than uranium (U 92) are generally not found in nature, and don't hang around for long if they are man-made.
\item Uranium-235 is used for fission reactions. It is the only naturally-occurring atom where fission can readily occur. The fissionable plutonium must be manufactured.
\item Plutonium (Pu) has the atomic number 94. The fissionable isotope is plutonium-239. Plutonium-238 has been used in radioisotope thermoelectric generators that are installed on spacecraft. (They generate power for a really long time.)
\end{itemize}

\subsection{Mass-energy equivalence}
\label{sec:org445f30e}

\[E=mc^2.\]

\begin{itemize}
\item Albert Einstein discovered the mass-energy equivalence in the early 1900s. Mass is simply congealed energy.
\item The c here is the speed of light.
\item Key to understanding how energy is released in nuclear reactions.
\begin{itemize}
\item The more energy associated with a particle, the greater its mass will be.
\end{itemize}
\item Atoms heavier than iron give off energy when their nuclei break apart, atoms lighter than iron just absorb energy.
\item Nuclear mass: As the atomic number of elements increases, the elements are increasingly more massive. If you graph this, it is a slight upward curve rather than a perfect straight line because the more massive atoms have a larger proportion of neutrons than the smaller atoms.
\item Mass per nucleon: results from the division of a nucleus' mass by the number of nucleons. You lose some energy with a nucleon when it is bound to a nucleus rather than existing on its own. Hydrogen has the highest mass per nucleon because it doesn't have binding energy to pull this mass down.
\item This doesn't need to make sense, shut up and calculate.
\end{itemize}

\subsection{Nuclear Fusion}
\label{sec:orge19df59}
\begin{itemize}
\item Nuclear fission releases energy by splitting atoms. \emph{Fusion} releases energy by combining two light atoms together into a heavier nucleus.
\item Energy is released if the light atoms are fused to create a nucleus that is lighter than iron. If the product of fusion is heavier than iron, then you actually consume energy.
\item Release of energy = reduction in mass.
\item In order to have fusion, you need to overcome the electrical repulsion natural between protons. This requires high pressure and temperatures to squish nuclei close together.
\begin{itemize}
\item We haven't been able to make fusion reactors, but we have made fusion bombs (also called H-bombs).
\end{itemize}
\end{itemize}

\section{January 29: Electromagnetism}
\label{sec:org1d42a95}

\subsection{Electric Force and Charge}
\label{sec:org631adaa}
\begin{itemize}
\item Electric force is a fundamental force of nature that can attract or repel things.
\item Charge is a physical property of matter that causes the force. You have positive charge or negative charge. Neutral is an absence of charge.
\item Electromagnetism affects all things that have a charge, and it is one of the four fundamental forces of nature. It goes alongside gravity (affects all things with mass), the strong nuclear force (affects protons and neutrons when they are very close together), and the weak nuclear force (affects protons, neutrons and electrons, and controls beta decay).
\item There is a fundamental rule for electricity: like charges repel and opposites attract.
\item Protons have positive charges, electrons have negative charges and neutrons have neutral charge.
\item As we have seen, the strong nuclear force is much stronger than electric repulsion, because it overcomes that by holding protons together inside a nucleus.
\item When we talked about radiation, our focus was on nucleons. When we talk about electricity, our focus is on electrons.
\item An atom is normally neutral, with the same number of electrons outside the nucleus as protons inside.
\item Conductive materials, like metals, have numerous electrons that are loosely bound and are able to move around freely and from atom to atom.
\begin{itemize}
\item When an atom loses one or more electrons, it becomes a positive ion. When it gains one or more electrons, it becomes a negative ion.
\item While it's a lot easier to pull an electron off an atom then it is to pull a proton, you do still require some work to do it. It is easy to do with metal atoms and other conductors, and harder with insulators, because their electrons are tightly bound.
\item Example: It is relatively easy for Florine to gain an electron, making it a negative ion called lithium. We then write \(F^-\) to indicate this, because it gained one negative charge. If it gained more, a number is included. The number of new electrons you can add to an atom depends on how much room the outer energy level has room for.
\item Metals do not typically become negative ions because they don't hold onto extra electrons very well. Non-metals actually become more stable when the outer energy level is full, so they tend to become negative ions and stay that way.
\item Aluminum for example only has three electrons on its outer energy level, and those are easily lost.
\end{itemize}
\item Motivating a flow of electricity within a material requires force.
\end{itemize}

\subsection{Static Electricity}
\label{sec:org6f9ae37}
\begin{itemize}
\item Not the creation of a charge, but simply due to a difference in charges.
\item You stroke a cat's hair, the hair can lose electrons and gain a positive charge, while the hand becomes negative.
\item Induction: A charged object is brought to a neutral object, it induces an opposite charge to the neutral thing. This is why when you rub a balloon on your head and cause it to have a positive charge, it will stick to a neutral wall.
\item You rub your feet on a carpet, the carpet loses electrons to your body. You can then have a negative charge and touching something (like a doorknob or a person) will induce a positive charge on the other thing, and push the charge over.
\end{itemize}

\subsection{Conservation of Charge}
\label{sec:org4236608}
\begin{itemize}
\item Charge is not created or destroyed, but electrons are transferred from one object to another.
\end{itemize}

\subsection{Coulomb's Law}
\label{sec:orgaf840c8}
\begin{itemize}
\item For two charged objects whose size is smaller than the distance between them, force between them varies directly as expressed in this equation. It is similar to Newton's Law.
\end{itemize}

\[F=k\left(\dfrac{q_1 q_2}{d^2}\right).\]

\begin{itemize}
\item k is a proportionality constant, \(9.0 \cdot 10^9 N \cdot m^2/C^2\).
\item q is the symbol for electric charge.
\item If the sign of the answer is negative, then the force is attractive. If the sign is positive, then the force is repulsive.
\item The unit for charge is coulombs, c. All electrons have a fundamental charge that is \(1.6 \cdot 10^{-19}\) coulombs.
\end{itemize}

\subsubsection{Similarities and Differences to Newton's Law}
\label{sec:orgf67ce54}
\begin{itemize}
\item Gravity only attracts, but electricity can repel or attract.
\item Both forces act across distances.
\item They both use the inverse square law.
\end{itemize}

\subsection{Polarization}
\label{sec:orge3c2653}
\begin{itemize}
\item Sometimes, a molecule does not have evenly distributed charge. We call this distortion of charge ``electric polarization''.
\item Polarization refers to when you have displaced positive or negative charge on a neutral object. You can create it by induction.
\item If something is overall neutral, but one end is positive and the other is negative in the same amount, then it is polarized.
\end{itemize}

\subsection{Electric Fields}
\label{sec:org37db82a}
\begin{itemize}
\item An electric field is a region where an electric force exists. It surrounds any charged object.
\item It is mostly a mathematical way to describe electric forces.
\item In most situations, you have a lot of charged particles in a region, and it would be hard to use Coulomb's Law to analyze the force between them, because the law only works for two things at a time.
\item So we say the electric field around them is constant.
\item Electric fields are vectors. The magnitude of the field at any point is stated as a force per unit charge.
\item Fields obey the inverse square law.
\item Field lines are used to visualize an electric field. They show direction away from positive and toward negative. They also show intensity of the field --- many bunched-together lines represent a strong part of the field, while farther-apart lines represent a weaker area in the field.
\end{itemize}

\subsection{Electric Potential Energy}
\label{sec:org723deb9}
\begin{itemize}
\item Charged particles can have electric potential energy due to its location within an electric field.
\item Similar to how an object can have gravitational potential energy due to its relative height. Bowling ball on a shelf has potential energy, and converts this to kinetic energy when it falls down on Uncle Buck.
\item Batteries and generators do work to pull negative charges away from positive charges. The work becomes available to a circuit as electrical potential energy.
\item For a battery, there are chemical reactions that happen side which move electrons from one electrode with a positive charge (anode) to another with a negative charge (cathode). The battery acid is called an electrolyte.
\item When you re-charge a battery, you reverse the chemical process.
\end{itemize}


\subsection{Electric Potential (Voltage)}
\label{sec:org1696eac}
\begin{itemize}
\item Electric potential is not the same as electric potential energy. Electric potential is the electric potential energy per charge, and it refers to the energy that a source provides to each Coulomb of charge.
\end{itemize}

\[\text{Electric potential} = \dfrac{\text{electric potential energy}}{\text{charge}}.\]

\begin{itemize}
\item The unit is then joules per coulomb, which we call Volts.
\item We can then say that voltage is a measure of energy density. The more volts a battery delivers, the more energy it has inside of it.
\end{itemize}

\section{Feb 5: Electrical Current}
\label{sec:org6033e3f}

\subsection{Current}
\label{sec:orgacf0de6}
\begin{itemize}
\item Electric current is often thought of as being similar to water flow in a pipe. You pump water through a pipe. But what is really happening with current in a wire is, you are applying a voltage to the wire and pushing electrons that already exist in the metal through the wire towards the positive charge.
\begin{itemize}
\item In a metal, current is the flow of electrons.
\item In fluids, you can have positive and negative ions flowing.
\end{itemize}
\item Potential difference (voltage) happens whenever the ends of an electrical conductor have different electric potentials. As noted in a previous lecture, voltage is a measure of energy density of an electric charge. Each Coulomb of charge has a certain amount of energy it can supply when it moves from negative to positive.
\item Voltage sources can include batteries, generators and alternators.
\item Charges in a conductor tend to flow from higher potential (negative) to lower potential (positive).
\item You can also say that current flows from positive to negative. This is a remnant of a different, earlier view on electric current when we kinda assumed electricity flowed from positive to negative. This way of thinking is called \href{https://www.mi.mun.ca/users/cchaulk/eltk1100/ivse/ivse.htm}{``conventional current''} and is actually still used quite widely today in engineering. Whether you say current flows from negative to positive or vice-versa, the result is the same.
\item You can view an electrical circuit as being similar to a water circuit. Water flows from high pressure to low pressure.
\item It takes energy to make a charge flow through a circuit. When a current encounters resistance, some energy is lost and converted into heat. Resistance is the opposition of current flow. It is measured in ohms, using the capital Greek letter omega, \(\Omega\). In a normal circuit there is always some resistance. (In superconducting circuits kept in a very cold environment, it is possible to achieve zero resistance, though.)
\item Electric potential difference is created in batteries through chemical reaction. Zinc or lead is disintegrated in acid, and the energy stored in chemical bonds is converted to electric potential energy.
\item Current is measured in amperes, or amps, which is the rate of flow of 1 coulomb of charge per second.
\item The actual speed of electrons in a conductor is quite flow, but an electric signal travels close to the speed of light because of a chain effect of pushing electrons.
\end{itemize}

\subsection{Types of Current}
\label{sec:org19417c6}
\begin{itemize}
\item DC is direct current. Charge flows in one direction. A graph of current vs time is a straight line.
\item AC is alternating current, and charges change direction constantly. The graph is a sinusoidal wave. When the curve goes below the x-axis, the current becomes negative and changes direction.
\begin{itemize}
\item Alternating current is created by an alternator by periodically switching the sign at the terminals.
\end{itemize}
\item Semiconductor circuits need direct current, so when you plug many electronics into the wall, they have an inverter that converts the current to DC and steps down the voltage.
\end{itemize}

\subsection{Ohm's Law}
\label{sec:org90abdee}

\begin{itemize}
\item A linear approximation that relates current, voltage and resistance. Ohm's law states that the current through a conductor is directory proportional to the voltage across the conductor, and inversely proportional to its resistance.

\[I=\dfrac{V}{R}.\]

\item Where I is current, V is voltage and R is resistance. Resistance is held constant in this statement of the law.

\begin{itemize}
\item Another requirement of Ohm's law is that temperature is held constant. If it were not, then we couldn't assume that resistance is also constant.
\end{itemize}

\item Derived experimentally by the German physicist George Ohm and published in 1827.

\item Ohm's law is not universal.

\begin{itemize}
\item Devices/circuits/components that obey Ohm's law are called ohmic. Things that do not follow Ohm's law are called nonohmic.

\item Ohm's law may not hold for strong field (i.e., when the voltage applied is high and the flow heats the conductor.)
\end{itemize}
\end{itemize}


\subsection{Socket-Plug Design}
\label{sec:org9e0eda5}

\begin{itemize}
\item The typical North America electric plug has two prongs. Usually one is a little wider than the other (in this case it is called a polarized plug). One is the ``live'' prong and the other is ``neutral''. The neutral is connected to ground.
\item Sometimes you get a plug that is not polarized. You can plug these in whichever way you want, and the device will still work. But if you plug it in the wrong way, the internals of the device will be connected to the live wire when they aren't supposed to be. This is why sometimes the plug comes polarized, so you can only plug it in one way. Usually the plug will be polarized if it is important for safety or the operation of the device that the live and neutral prongs go in the correct way.
\item You can also have the ground prong, which is longer than the other two prongs so it's the first to be plugged in. Provides a direct path to ground to prevent any defects in the appliance from harming the user. It is not normally live, unless there is a defect in the appliance of course (think loose wire touching the metal casing of the appliance, for example.)
\item You don't need the ground if the casing of the appliance has appropriate insulation so as not to shock the user if there was anything wrong with the device.
\end{itemize}

\subsection{Factors Affecting Resistance}
\label{sec:org19b3cca}

\begin{itemize}
\item One is material. Using a good conductor will reduce resistance.
\item Temperature. Higher temperatures mean atoms are moving more in a material. This impedes flow of electrons, thus increasing resistance.
\item Conductor geometry. Thicker wires pose less resistance. Length is also important. Longer wires have more resistance.
\item Superconductors. To be superconductive means a material has zero resistance. Some metals can be superconductive at very low temperatures (not too far from absolute zero). Ceramic compounds can be superconductive at higher temperatures.
\item Semiconductors. Materials whose resistance can be varied by adding small amounts of impurities. They are controlled by applying a voltage. Semiconductors are the basis of electronics. We make electronics work by varying current inside a circuit.
\begin{itemize}
\item Semiconductors are metalloids like silicon and germanium.
\end{itemize}
\item Important to note that resistance does not \emph{really} depend on the current, except in the case where you have so much current that the conductor starts to heat up, which will of course affect resistance.
\end{itemize}

\subsection{Series and Parallel Circuits}
\label{sec:orgfcf3ba7}
\begin{itemize}
\item A series circuit has everything in line with each other. There is only one path for electrons to flow. You add up the total resistance for the whole circuit. The current is the same throughout.
\item In a parallel circuit, electrons have many complete paths to flow through. A device in each branch operates independently from the others. You add up the current across each branch to get the total current.
\begin{itemize}
\item In a parallel circuit, the total resistance is less than the resistance in each branch. Adding more branches lowers the total resistance.
\end{itemize}
\item Most real circuits are some combination of both series and parallel circuits.
\item Homes are wired in parallel. As more devices get connected, you run the risk of overloading the branch and overheating the breaker, which trips it.
\end{itemize}


\section{Feb 22 --- Week 5}
\label{sec:org1914d58}

Series and parallel circuits.

\subsection{Power}
\label{sec:orgb456efa}

\begin{itemize}
\item Power is the product of electric potential energy and current. It is the rate at which electric energy is converted into another form.

\[P=VI.\]

\begin{itemize}
\item Power is measured in watts, which are defined as Joules per second.

\item Note that watt hours are a measure of energy, not power.
\end{itemize}
\end{itemize}

\subsection{Series and Parallel Circuits}
\label{sec:orgb7f15fe}
\begin{itemize}
\item In a series circuit, there is only one path for current to flow.
\begin{itemize}
\item A break anywhere in the circuit will halt flow.
\item Current is the same everywhere in the circuit.
\end{itemize}
\item A parallel circuit has multiple paths. A break does not necessarily mean everything will stop working.
\item Combination circuits are more common. They have some elements that are series and other elements that are parallel.
\end{itemize}

\subsection{Circuit Analysis}
\label{sec:orga810ef7}
\begin{itemize}
\item Means to find the current, voltage and resistance through each circuit element.
\item Three tools:
\begin{itemize}
\item Ohm's law.
\item Kirchoff's current law.
\item Kirchoff's voltage law.
\end{itemize}
\end{itemize}

Kirchoff's current law

\begin{itemize}
\item At any junction in a circuit, the current entering and leaving the junction is equal.
\end{itemize}

Kirchoff's voltage law

\begin{itemize}
\item In a closed loop, sum of voltage gains equal sum of voltage drops.
\end{itemize}

Other rules

\begin{itemize}
\item The resistance of a series is all resistance added up: \(R_T = R_1 + R_2 + ...\).
\item For resistance in a parallel circuit, you can find total resistance by adding up the reciprocals of all the resistors: \(\frac{1}{R_T} = \frac{1}{R_2} + \frac{1}{R_2} + ...\).
\end{itemize}

\section{Magnetism}
\label{sec:org1616466}

\begin{itemize}
\item Magnetism isn't separate from electricity. Rather, it is another aspect of it. Hence, electromagnetism.
\item Magnetic poles are in all magnets. There are no magnetic monopoles.
\item Magnetic force: opposites attract and likes repel. Follows the inverse square law.
\item Magnetic fields are around a magnet, and are produced by moving electrical charges.
\item Field spreads from pole and curves around the magnet, returning to the other pole.
\item Drawings of magnetic fields will indicate where the field is strong by putting little lines really close together. Areas where the lines are less dense mean the field is weaker there.
\item Iron filings become induced magnets if you align them so that their north and south poles line up with the magnetic field. But once they are lined up parallel to the direction of the field, the force disappears.
\item ``It turns out magnetism is weird''.
\item A compass has a magnetic needle that aligns with the Earth's magnetic field. Once it aligns, the force stops.
\item Magnetic fields are produced by two kinds of electron motion.
\begin{itemize}
\item Electrons have a property known as spin. They can spin up or spin down. If a pair of electrons spin in the same direction, the magnetic field becomes stronger. If a pair of electrons spin in opposite directions, they cancel out each other's fields. For example, copper has no magnetic fields because its electrons are spinning in opposite directions. Iron, on the other hand, is quite magnetic (in fact it has the strongest magnetic effects of any element).
\item Whether or not an atom will be magnetic depends on its number of electrons, and which orbitals they are in.
\end{itemize}
\item Magnetic domains are clustered regions of aligned atoms. When you have a material, like steel, it may or may not be magnetic, despite containing elements that have strong magnetic effects. That is because the domains are not always aligned in the same direction, so cancel out the fields of each other. It is possible to manufacture a material to get a desired magnetic effect. To make a magnet, you would solidify molten metal inside of the field of another magnet. This makes the domains align in a preferred direction.
\begin{itemize}
\item However, regular iron alloys don't stay magnetized for long. The atoms can lose alignment because atoms can rotate within the solid structure. To make a permanent magnet requires a special allow where the atoms can't rotate. AlNiCo is a common alloy.
\end{itemize}
\end{itemize}

\subsection{Electric Current and Magnetic Fields}
\label{sec:org41d83e3}

\begin{itemize}
\item The Earth is magnetic because of molten iron in its core. It is believed that the flow of magma in convection currents results in an electric current that creates the magnetic field of the Earth.
\begin{itemize}
\item Earth's magnetic field deflects many charged particles that make up cosmic radiation, thereby protecting us from harmful exposure.
\item The effects of cosmic background radiation must be taken into account in the design of electronics that will be operated outside of earth's protective magnetic field.
\end{itemize}
\item Electrons moving in a wire creates a magnetic field. Around the wire will be loops of magnetic field.
\item The stronger the current, the more powerful the magnetic field.
\item An electromagnet can also be made stronger by increasing the number of loops per unit of wire length. (Actually increasing the length of the wire, we know would reduce current so that would weaken the strength.)
\item In the LHC, a beam of protons is directed around a 27km circle using superconducting electromagnets, cooled by liquid helium. These electromagnets use very thin wiring to create a strong magnetic effect.
\item Force on a moving charge is calculated with the Lorentz Force Law.
\end{itemize}

\[F=QvB.\]

\emph{Where Q is charge, u is velocity and B is strength of the magnetic field (measured in Teslas).}

\begin{itemize}
\item The direction of force on a moving charge is perpendicular to both the magnetic field direction and charge velocity. If the magnetic field and the direction the charged particle is moving in are parallel, there is no force.
\item The force can't change the speed of the charged particle, but it can change the direction component of its velocity.
\end{itemize}


\section{Electromagnetism Continued (March 12)}
\label{sec:org4ccc2d5}

\subsection{Electromagnetic Induction}
\label{sec:org8588ca6}

\begin{itemize}
\item Discovered by Michael Faraday and Joseph Henry. A change in magnetic field strength induces a voltage in a coil of wire. The field strength is changed by moving the magnet.
\item The induced voltage can be increased by:
\begin{itemize}
\item Increasing the number of loops in the coil.
\item Increasing the speed of the magnet entering and leaving the coil. Rapid motion produces more voltage.
\item Using a more powerful magnet.
\end{itemize}
\item Induction occurs whether the magnetic field moves past the wire or if the wire moves through the field.
\item The induced voltage in the coil produces its own electromagnetic field, which opposes the motion of the magnet. The coil becomes a magnet with opposite polarity (Oersted's Law).
\item An electric field is induced in any region of space in which a magnetic field is changing. Similarly, a magnetic field is induced in any region of space in which an electric field is changing with time.
\end{itemize}

\subsection{Faraday's Law}
\label{sec:orgd54e217}

\begin{itemize}
\item The induced voltage in a coil is proportional to the number of loops, multiplied by the rate at which the magnetic field changes within those loops.
\item The current produced depends on coil resistance, the circuit it connects, and the induced voltage.
\end{itemize}

\subsection{Generators and Alternators}
\label{sec:org259dfd8}
\begin{itemize}
\item Electromagnetic induction is the basic principle of an electric generator, as well as motors.
\item Alternators convert mechanical energy into electrical energy via coil motion. A coil spins within a magnetic field. The voltage of the current produced alternates.
\item The coil connects to a commutator.
\item A DC generator avoids negative voltage by connecting to the circuit differently.
\item Power converters use capacitors to change AC to DC. Semiconductors require DC power to work.
\item Generators usually have a moving coil because it's easier, but they can also have a moving magnet.
\item Using Faraday and Henry's discovery, Nikola Tesla and George Westinghouse showed that electricity can be economically generated with AC to light cities.
\end{itemize}

\subsection{Commutators}
\label{sec:org4386c62}

\begin{itemize}
\item An important component of electric generators and motors.
\item A shaft with coils of wire around it rotates in a magnetic field. On the shaft there is a layer of conductive material with discontinuities halfway around. It's like a ring, except with an insulating layer that splits it in halves. This is the commutator. It's polarized --- one half is positive, the other half is negative.
\item Mounted to the commutator are two brushes. As the shaft spins, and the commutator/brushes with it, the brushes make contact with the circuit.
\item It has to be set up so that when brush A touches one of the wires, brush B must also be touching the other wire.
\end{itemize}

\subsection{Transformers}
\label{sec:org8b70df7}
\begin{itemize}
\item A transformer boosts or lowers voltage of AC.
\item An iron core with two coils of wire around it, they do not share an electrical connection.
\item Input coil of wire is called the ``primary'' and is powered by an AC voltage source. This induces a changing magnetic field.
\item The induced magnetic field from the primary then creates a voltage in the secondary coil.
\item If the secondary coil has more loops than the secondary, the voltage steps up. If the primary has more loops than the secondary, the voltage steps down.
\end{itemize}

\[\dfrac{\text{primary voltage}}{\text{number of primary turns}} = \dfrac{\text{secondary voltage}}{\text{number of secondary turns}}.\]


\begin{itemize}
\item A neighbourhood transformer typically converts 2400V to 240V. There are three wires coming off it. Two wires provide 120V, and adding in the third give you 240V. Houses use both --- 240V is for major appliances like stoves and dryers. The 240V supply has lower amperage than 120V.
\item A transformer only changes voltage. If voltage goes up, current goes down.
\end{itemize}

\subsection{Field Induction}
\label{sec:orgf953b59}
\begin{itemize}
\item Light is produced by the mutual induction of electric and magnetic fields.
\item Electromagnetic fields always travel at the speed of light.
\end{itemize}
\end{document}
