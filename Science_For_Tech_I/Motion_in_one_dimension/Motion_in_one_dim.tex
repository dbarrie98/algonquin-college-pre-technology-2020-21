\documentclass[10pt]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{microtype}
\usepackage{amsfonts}
\usepackage{mathptmx}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{sidecap}
\usepackage{float}
\usepackage[backend=biber,style=apa,autocite=inline]{biblatex}
\DeclareLanguageMapping{english}{english-apa}
\addbibresource{physicssources.bib}
% By default, an article has some vary large margins to fit the smaller page format.  This allows us to use more standard margins.
\setlength\bibitemsep{\baselineskip}
\setlength{\parskip}{1em}
% This gives a full line break with every new graf. 1em is the length of a capital M
\title{Motion in One Dimension \\\medskip
\large Review Notes for SCI5900 --- Prof. James McLaren}
\author{Devyn Barrie}
\date{October 19, 2020}
\begin{document}
\maketitle

\tableofcontents 

\section{Newton's Laws}

Quoted from Wolfram Alpha:

\nocite{WolframAlpha}

First law: ``A body persists in a state of rest or of uniform motion unless acted upon by an external unbalanced force."

Second law: ``When mass is constant, force equals mass times acceleration ($F = ma$): the net force on an object is equal to the mass of the object multiplied by its acceleration." (Meaning that more mass = less acceleration, while more force = more acceleration. Acceleration and force are vectors and occur in the same direction.)

Third law: ``Whenever a particle A exerts a force on another particle B, B simultaneously exerts a force on A with the same magnitude in the opposite direction."
\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{980abc65fc7ee97f974054a57bc05d0a.png}
\caption{\parencite{McLaren2020}}
\end{figure}

Question: A baseball pitcher throws a 91mph fastball. If the action is the pitcher pushing against the ball when he throws it, what is the opposite action?

A: It is the ball pushing on the pitcher's hand.

\section{Motion}

Impulse changes momentum.

\[I=Ft.\]

Where I=impulse, F=force and t=time.

Questions about change in momentum are really about impulse. 

Momentum describes how much mass is in how much motion. It is conserved. Only an outside force can change the momentum of a closed system.

\[p=mv.\]

Where p=momentum, m=mass and v=velocity.

A change in $p$ can also be written as $\Delta p=m \cdot \Delta v = F \cdot \Delta t.$

Difference between velocity and speed. Both are measured in the same units (like $km\cdot h^{-1}$) but speed is a scalar while velocity is a vector. Speed is just how fast something moves. Average speed is calculated with$\frac{distance\ traveled}{time\ interval}.$ Velocity is what we get when we specify a direction to speed. The equation for average velocity is more formally stated, as $\bar v = \frac{\Delta d}{\Delta t}$.

Acceleration: ``[Is] the name we give to any process where the velocity changes. Since velocity is a speed and a direction, there are only two ways for you to accelerate: change your speed or change your direction—or change both." \parencite{Khan2017}

The equation for average acceleration is $\bar a = \frac{\Delta v}{\Delta t}$.

\section{Energy and Work}

Energy and work are very closely related. Energy is the ability to do work. Meanwhile, work is defined as a transfer of energy by applying force over distance.

\begin{itemize}
\item Potential energy (energy of a system due to its relative position, for example a bowling ball stored on a shelf has potential energy that can be converted to kinetic energy if it were to fall.)
\item Kinetic energy (the energy of a system due to its motion)
\item Pressure energy (``energy in/of a fluid due to the applied pressure (force per area)"\parencite{stack}
\item Mechanical energy, which is the sum of potential, kinetic and pressure energies of a fluid. \parencite{fluids}
\item Thermal energy, the energy of a system that is associated with its temperature. 
\item Chemical energy, released by means of a chemical reaction.
\item Sound energy, the sum of potential and kinetic energy in the form of a soundwave.
\end{itemize}

Energy is measured in joules, which is SI-derived from $kg\cdot m^2\cdot s^{-2}$. Since work is also measured in joules, you can see they are essentially equivalent. In fact, the work-energy theorem relates these two concepts by stating that the net work done on a system is equal to the change in kinetic energy for the system. $W_{net}=\Delta KE.$

What's work? As stated by professor McLaren: ``When you transfer energy, you're doing work" \parencite{McLaren2020_2}.

Mathematically, work is:

\[W=Fd.\]

Where W is work, F is force and d is distance.

\subsection{Work vs Energy Example}

Example: I crack open a beer while working on physics homework. I have expended a small amount of energy (chemical, from food I ate) and converted it into the kinetic energy of a finger movement on the can lid. This kinetic energy is transferred to the can lid, doing work on it and thus opening it. The energy transferred is then dissipated by a number of avenues, such as heat and sound energy. 

Interestingly, at least on paper, I didn't do any work while walking to the fridge. Walking along a level surface at a constant speed will not result in work being done because my net force is zero (my speed is constant, friction with the ground is the equal and opposite force). As a result $W=(0)d =0$. If my speed was variable (maybe I am drunk) then perhaps some work will have been done after all. 

\subsection{More Energy Terms}

Power is defined as work done over time. It is measured in watts, which are $Js^{-1}$. The same amount of work can be done quickly or slowly, but may require different amounts of power \parencite{McLaren2020_2}.

Confusingly, you also have the energy measurements like kWh. These are not power measurements. Always remember that the definition of a watt already includes the time component. If it appears with another time component, then you are no longer measuring power. 

Potential energy is defined as the energy a system possesses as a result of its relative position. Gravitational potential energy is due to a system's height. You can also have potential energy in the form of a tight spring, wherein the energy may be released in the form of kinetic energy if the spring were released. For gravitational potential energy, the equation is $PE=mgh$, where m is mass, g is the gravitational constant ($~10$) and h is the height. 

Kinetic energy is the energy of a system due to its motion. The equation is $KE=\frac{mv^2}{2}.$ Note that velocity is squared, so it has a bigger impact on the value of kinetic energy than mass. 

Energy is conserved, meaning the universe never loses or gains any energy. However, energy is never fully converted from one form to another without some loss to another form. Example: Cars are intended to convert chemical energy into kinetic energy, but in doing so also create a tremendous amount of thermal and sound energy that does not all go towards moving the car. From a practical perspective, the energy is ``lost" in that it leaves the system via the tailpipe. It is not just due to mechanical imperfections that this occurs. The 2nd law of thermodynamics guarantees that even a 100\% perfect, friction-free heat engine could never be 100\% efficient due to entropy generation. (Read up on the history of the Carnot cycle for more about this.)

\newpage

\printbibliography
\end{document}


